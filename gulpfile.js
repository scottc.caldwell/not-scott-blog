const {parallel, watch} = require('gulp');

const sass = require('./gulp-tasks/sass.js');
const scripts = require('./gulp-tasks/scripts.js')

const watcher = () => {
  watch('./src/sass/**/*.scss', {ignoreInitial: true}, sass);
  watch('./src/posts/**/*.scss', {ignoreInitial: true}, sass);
  watch('./src/scripts/*.js', {ignoreInitial: true}, scripts);
  watch('./src/posts/**/*.js', {ignoreInitial: true}, scripts);
};

exports.default = parallel(sass, scripts);
exports.watch = watcher;