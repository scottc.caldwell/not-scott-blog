const {dest, src} = require('gulp');
const babel = require('gulp-babel');
const minify = require('gulp-babel-minify');

const isProduction = process.env.NODE_ENV === 'production';

const scripts = () => {
  return src('./src/*.js')
  .pipe(babel({
    presets: [[
      "@babel/env", {
        "targets": {
          "edge": "17",
          "firefox": "60",
          "chrome": "67",
          "safari": "11.1",
        },
      }
    ]]
  }))
  // .pipe(minify({
  //   mangle: {
  //     keepClassName: true
  //   }
  // }))
  .pipe(dest('./src/_includes/js'));
};

module.exports = scripts;
