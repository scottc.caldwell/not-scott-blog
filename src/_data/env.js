const environment = process.env.NODE_ENV;
const PROD_ENV = 'production';
const prodUrl = 'https://notscott.dev';
const devUrl = 'http://localhost:8080';
const baseUrl = environment === PROD_ENV ? prodUrl : devUrl;
const isProd = environment === PROD_ENV;

module.exports = {
  siteName: 'notscott.dev',
  environment,
  isProd,
  baseUrl,
  tracking: {
    gtag: 'UA-83445804-3',
  },
};