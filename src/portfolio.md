---
layout: 'layouts/portfolio.html'
footerText: "Designed and developed by Scott Caldwell"
title: Portfolio
metaDesc: Portfolio of Scott Caldwell, front-end web developer based in Vancouver, BC, Canada
noindex: true
---
<!-- 
TODO: move all the text/img content into frontmatter,
      add partials to portfolio.html template,
      so it's easier to add more content in the future
 -->