// -------------------------------------
// Logo animation
// -------------------------------------
const animateLogo = () => {
  const logo = document.getElementById("notscottLogo");

  const not = logo.getElementById("not");
  const scott = logo.getElementById("scott");
  const dotDev = logo.getElementById("dotDev");
  const crossbar = logo.getElementById("crossbar");
  const crossbarLength = crossbar.getBBox().width;
  const scottLength = scott.getBBox().width;
  const notScottGap = scott.getBBox().x - not.getBBox().width;
  const crossbarDeltaX =  -scottLength - notScottGap;
  const crossBarScaleAmount = -(crossbarDeltaX - crossbarLength) / crossbarLength;
  
  const tl = new TimelineMax();
  tl.timeScale(1.75);
  tl.add("begin");
  
  //stretch crossbar as strikethrough effect
  tl.to(crossbar, {
    duration: 2,
    scaleX: crossBarScaleAmount,
    ease: "power2.out",
    transformOrigin: "center right"
  }, "begin");
  
  //and unstretch to become t crossbar
  //and also move, so it ends up on t of "not"
  tl.to(crossbar, {
    duration: 1,
    scaleX: "1",
    x: crossbarDeltaX,
    ease: "power2.out"
  }, "begin+=1");
  
  //in reverse, show letters of "not"
  tl.to("#not .letter",{
    opacity: 1,
    stagger: {
			amount: -0.5
		}
  }, "begin+=1");
  
  //afterwards, 'type' out final letters
  tl.to("#dotDev .letter",{
    delay: 1,
    opacity: 1,
    stagger: {
			amount: 0.5
		}
  }, "begin+=1");
};

document.addEventListener("DOMContentLoaded", function(){
  setTimeout(animateLogo, 1000);
},{once:true});
