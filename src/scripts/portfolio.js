gsap.registerPlugin(ScrollTrigger)

function cycleTitles(){
  var tl = new TimelineMax({repeat:-1});
  var numTitles = document.getElementsByClassName("hero__career-title").length - 1;
  
  for (var i = 0; i < numTitles; i++){
    if(i==numTitles){
      tl.to(".hero__career-title", 0.4,{
        y: '-=100%',
        delay: 0,
        ease: Sine.easeOut
      });
    }
    tl.to(".hero__career-title", 0.4,{
      y: '-=100%',
      delay: 2,
      ease: Sine.easeOut
    });
    
  }
}

function slideElements(element){
  gsap.utils.toArray(element).forEach(el => {
    gsap.from(el, 0.4, {
      y: '100%', 
      opacity: '0',
      scrollTrigger: {
        trigger: el,
        ease: Sine.easeOut,
        start: 'center 95%'
      }
    });
  });
}

// -------------------------------------
// Enable Flickity carousels
// -------------------------------------
const carouselElements = document.querySelectorAll('.carousel');
carouselElements.forEach(carouselElement => {
    var flkty = new Flickity( carouselElement, {
        prevNextButtons: false
        // freeScroll: true,
        // freeScrollFriction: 0.03
    });

    //add scroll trigger to each element
    ScrollTrigger.create({
      trigger: carouselElement,
      start: 'center bottom',
      onEnter: self => flkty.playPlayer(),
      onLeave: self => flkty.pausePlayer(),
      onEnterBack: self => flkty.unpausePlayer(),
    });
});

// -------------------------------------
// Trigger GSAP animations, unless 'prefers-reduced-motion' media query is set to reduce
// -------------------------------------
const reducedMotionPreferred = window.matchMedia('(prefers-reduced-motion: reduce)').matches;
if (!reducedMotionPreferred) {
  slideElements('.card__company-logo');
  cycleTitles();
}
