let app = new Vue({
  el: '#app',
  data: {
    cubifyText: 'Type some text'
  },
  filters:{
    cubify: cubify
  },
  methods: {
    copyText() {
      let originalText = this.cubifyText;
      let text = this.$options.filters.cubify(this.cubifyText);
      navigator.clipboard.writeText(text);
      this.cubifyText = "duplicated";
      setTimeout(()=> {
        this.cubifyText = originalText;
      }, 1000);
    }
  }
})

//credit to /u/lumbdi and spread by their bot u/LinLeyLin
//converted to JS from python script --> https://github.com/NNTin/Cubify-Reddit
function cubify(input){

    if ((input.length < 5)){
      const tooShortText = 
      `Word must be at least 5 characters to cubify

        T O O   S H O R T   
      / O             / O   
    /   O           /   O   
  /               /         
T O O   S H O R T       S   
O       H       O       H   
O       O       O       O   
        R               R   
S       T O O   S H O R T   
H     /         H     /     
O   /           O   /       
R /             R /         
T O O   S H O R T  

      `
      return tooShortText;
    }  
    
    // if((input[0] !== input.slice(-1))){
    //   return 'First and last letter need to match'
    // }

    const slash = '/';
    let word = input.toUpperCase();
    let gap = Math.floor(word.length / 2);
    let correction = ((word.length % 2) === 0) ? 1 : 0;
    let size = (Math.floor( word.length / 2 ) + parseInt(word.length + 1));
    let matrix = createMatrix(size);


    for(let i = 0; i < word.length; i++){
      // write horizontal words
      matrix[0][gap + i] = word[i];                                           //back top 
      matrix[gap][i] = word[i];                                               //front top
      matrix[2 * gap - correction][gap + i] = word[i];                        //back bottom 
      matrix[3 * gap - correction][i] = word[i];                              //front bottom 

      //write vertical words
      matrix[gap + i][0] = word[i];                                           //front left 
      matrix[i][gap] = word[i];                                               //back left 
      matrix[i][3 * gap - correction] = word[i];                              //back right 
      matrix[gap + i][2 * gap - correction] = word[i];                        //front right 
    }

    for(let i = 1; i < gap; i++){
      //add slashes
      matrix[gap - i][i] = slash;                                             //top left 
      matrix[gap - i][2 * gap + i - correction] = slash;                      //top right 
      matrix[3 * gap - i - correction][i] = slash;                            //bottom left 
      matrix[3 * gap - i - correction][2 * gap + i - correction] = slash;     //bottom right
    }    

    //for each row in matrix, add space to each letter
    //for reddit formatting, add 4 spaces to start of each row
    for(let i = 0; i < matrix.length; i++){
        matrix[i][0] = '    ' + matrix[i][0];
        for(let j = 0; j < matrix.length; j++){
            matrix[i][j] += ' ';
        }
    }

    return displayMatrix(matrix);
}

function createMatrix(size){
    let matrix = [];
    //create matrix filled with placeholder
    for(let i = 0; i < size; i++ ){
        matrix[i] = [];
        for (let j = 0; j < size; j++) { 
            matrix[i][j] = ' ';
        }
    }
    return matrix;
}

function displayMatrix(matrix){
    let string = '';
    matrix.forEach(function(x){
        x.forEach(function(y){
            string += y;
        });
        string += '\n';
    });
    return string;
}
