class Arrow {
  constructor( x, y, column, row, color="black"){
    this.x = x;
    this.y = y;
    this.column = column;
    this.row = row;
    this.color = color;
    this.rotation = 0;
  }

  draw(context){
    context.save();
    context.translate(this.x, this.y);
    context.rotate(this.rotation);
    context.lineWidth = 2;
    context.fillStyle = this.color;
    context.strokeStyle = this.color;
    context.beginPath();
    context.moveTo(-50, -25);
    context.lineTo(0, -25);
    context.lineTo(0, -50);
    context.lineTo(50, 0);
    context.lineTo(0, 50);
    context.lineTo(0, 25);
    context.lineTo(-50, 25);
    context.lineTo(-50, -25);
    context.closePath();
    context.fill();
    context.stroke();
    
    context.restore();
  }
}

const captureMouse =  (element) => {
  let mouse = {
        x: 0, 
        y: 0, 
        event: null
  };
  document.addEventListener('mousemove', function(event){
    let canvas = document.querySelector('#canvas');
    let rect = canvas.getBoundingClientRect();

    mouse.x = event.clientX - rect.left;
    mouse.y = event.clientY - rect.top;
    mouse.event = event;
    return mouse;
  })
  
  return mouse;
};

const captureTouch = (element) => {
  let touch = {
    x: null,
    y: null,
    isPressed: false,
    event: null
  };

  element.addEventListener('touchstart', function (event) {
    touch.isPressed = true;
    touch.event = event;
  }, false);

  element.addEventListener('touchend', function (event) {
    touch.isPressed = false;
    touch.x = null;
    touch.y = null;
    touch.event = event;
  }, false);
  
  element.addEventListener('touchmove', function (event) {
    let x, y;
    let canvas = document.querySelector('#canvas');
    let rect = canvas.getBoundingClientRect();

    x = event.clientX - rect.left;
    y = event.clientY - rect.top;
    touchEvent = event;
    
    touch.x = x;
    touch.y = y;
    touch.event = event;
  }, false);
  
  return touch;
};

const fitToContainer = (canvas) => {
  // Make it visually fill the positioned parent
  canvas.style.width ='100%';
  canvas.style.height='100%';
  // ...then set the internal size to match
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
};

const clearCanvas = (canvas) => {
  const context = canvas.getContext('2d');
  context.clearRect(0, 0, canvas.width, canvas.height);
};



const addArrowsToCanvas = (canvas, arrowWidth, arrowHeight) => {  
  const columns = Math.trunc(canvas.width / arrowWidth);
  const rows = Math.trunc(canvas.height / arrowHeight);
  const numArrows = columns * rows;
  const xGap = (canvas.width - (columns *  arrowWidth)) / 2;
  const yGap = ((canvas.height - rows *  arrowHeight)) / 2;
  
  let createArrows = (col, row) => {
    const arrowArray = [];
    for (let x = 0; x < col; x++) {
      for (let y = 0; y < row; y++) {
        let xPos = xGap + (arrowWidth / 2) +  (x * arrowWidth);
        let yPos = yGap + (arrowHeight / 2) + (y * arrowHeight);
        
        let color="#F44336";
        if(y % 3 == 2){
          color = "#1E88E5"
        }
        if(y % 3 == 1){
          color = "#FDD835"
        }
        arrowArray.push(new Arrow(xPos, yPos, x, y, color)); 
      }
    }
    return arrowArray;
  }
  
  clearCanvas(canvas); 
  const arrows = createArrows(columns, rows);
  return arrows;
};

const canvas = document.getElementById('canvas');
const mouse = captureMouse(canvas);
const touch = captureTouch(canvas);
const context = canvas.getContext('2d');

fitToContainer(canvas);
let arrows = addArrowsToCanvas(canvas, 110, 110);

window.addEventListener("resize", ()=>{
  fitToContainer(canvas);
  arrows = addArrowsToCanvas(canvas, 110, 110);
});


(function drawFrame(){
  window.requestAnimationFrame(drawFrame, canvas);
 
  clearCanvas(canvas);
  arrows.forEach(function(arrow){
    let dx = (mouse.x || touch.x) - arrow.x;
    let dy = (mouse.y || touch.y) - arrow.y;

    arrow.rotation = Math.atan2(dy, dx);
    arrow.draw(context);
  })

  
})();