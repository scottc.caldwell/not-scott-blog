import { playerData } from './rocket-richard-data.js';
// -------------------------------------
// Init
// -------------------------------------

const SEASON_LENGTH = 82;

// -------------------------------------
// Insert data to page
// -------------------------------------
function addOverviewToDOM(playerArray){
  playerArray.map((player, index) => {
    let overviewSection = document.querySelector("#overview");
    let cards = overviewSection.children;
    let card = cards[index];
    let cardName = card.querySelector('.card__name');
    let cardGoals = card.querySelector('.card__goals');
    let cardStatsList = card.querySelector('.card__game-stats');
    let cardLogo = card.querySelector('.card__team-logo');
    let cardTeam = card.querySelector('.card__team');
    let cardPosition = card.querySelector('.card__position');

    // This shouldn't be here, but data fetching/parsing functions have been removed
    // and I don't want to hardcode these stats in
    let goalsPerGame = (player.totalGoals / player.gamesPlayed).toFixed(2);
    let statsListHTML = `<li class="card__stat">${player.gamesPlayed} <span>games played</span></li>
                        <li class="card__stat">${goalsPerGame} <span>goals per game</span></li>
                        `  
    card.classList.add(player.domId);
    if(player.isWinner){
      card.classList.add("card--winner");
    }
    cardGoals.insertAdjacentHTML("afterbegin", player.totalGoals);
    cardName.insertAdjacentHTML("afterbegin", player.name);
    cardTeam.insertAdjacentHTML("afterbegin", player.team);
    cardPosition.insertAdjacentHTML("afterbegin", player.position);
    cardLogo.src = `/assets/img/rocket-richard/${player.teamLogo}.svg`;
    cardLogo.alt = `${player.team} Logo`;
    cardStatsList.insertAdjacentHTML("afterbegin", statsListHTML);
    card.classList.remove("hidden");
  })
  return playerArray;
}

function getRaceChartConfig() {
  let width = 450;
  let height = 300;
  let margin = {
    top: 16,
    bottom: 48,
    left: 36,
    right: 0
  }

  let bodyHeight = height - margin.top - margin.bottom
  let bodyWidth = width - margin.left - margin.right 

  let container = d3.select("#raceChart");
  container
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", `0 0 ${width} ${height}`)

  return { width, height, margin, bodyHeight, bodyWidth, container }
}

function getRaceChartScales(playerArray, config) {
  let { bodyWidth, bodyHeight } = config;
  let maxGoals = d3.max(playerArray, player => player.totalGoals )

  let xScale = d3.scaleLinear()
    .range([0, bodyWidth])
    .domain([0,SEASON_LENGTH])

  let yScale = d3.scaleLinear()
    .range([0, bodyHeight])
    .domain([maxGoals + 5, 0])
    
  return { xScale, yScale }
}

function drawRaceChartLines(playerArray) {
  let config = getRaceChartConfig();
  let scales = getRaceChartScales(playerArray, config);
  let {container, margin, height} = config; 
  let {xScale, yScale} = scales;
  let maxGamesPlayed = d3.max(playerArray, player => player.gamesPlayed);
  let lineGenerator = d3.line()
    .x((d, index) => xScale(index))
    .y((d)=> yScale(d))

  let lines = container.append("g")
    .attr("class", "lines")

  playerArray.reverse().map( player => {
    let raceArray = player.sumArray;

    lines.append("line")
    .attr("class", "covid-line")
    .attr("x1", margin.left + xScale(maxGamesPlayed)) 
    .attr("y1", height - margin.bottom - 3)
    .attr("x2", margin.left + xScale(maxGamesPlayed)) 
    .attr("y2", margin.top);

    lines.append("text")
      .attr("class", "covid-label")
      .attr("transform", "rotate(-90)") //causes x/y to switch
      .attr("y", margin.left + xScale(maxGamesPlayed) + 16)
      .attr("x", 0 - (height / 2))
      .attr("text-anchor", "middle")
      .text("COVID-19");

    lines.append("g")
      .style("transform",`translate(${margin.left}px,${margin.top}px)`)
      .append("path")
      .datum(raceArray)
      .attr("class", "line " + player.domId)
      .style("mix-blend-mode", "multiply") // Do I need this? css
      .attr("d", lineGenerator)
      .on("mouseenter", function() {
        d3.select(this.parentNode).raise();
      })
  })
}

function drawRaceChartAxes(scales, config, playerArray){
  let {xScale, yScale} = scales
  let {container, margin, height, width} = config;
  let axisX = d3.axisBottom(xScale);
  let axisY = d3.axisLeft(yScale).ticks(7);

  container.append("g")
    .style("transform", `translate(${margin.left}px,${height - margin.bottom}px)`)
    .attr("id", "axisX")
    .call(axisX);

  container.append("g")
    .style("transform", `translate(${margin.left}px,${margin.top}px)`)
    .attr("id", "axisY")
    .call(axisY)

  // text label for the y axis
  container.append("text")
    .attr("class", "axis-label")
    .attr("transform", "rotate(-90)")
    .attr("y", 0)
    .attr("x",0 - (height / 2))
    .attr("text-anchor", "middle")
    .text("Goals");

  // text label for the x axis
  container.append("text")
    .attr("class", "axis-label")
    .attr("x", width/2)
    .attr("y", height - 8)
    .attr("text-anchor", "middle")
    .text("Games Played");
}

function drawRaceChartLegend(config, playerArray){
  let {container, margin, height} = config;

  let xPos = 26;//margin.left;
  let yPos = margin.top;

  let legend = container.append("g")
    .attr("class", "legend")
    .style("transform", `translate(${xPos}px,${yPos}px)`)
    
  playerArray.map((player, index) => {
    let nameYPosition = yPos + (20 * index);
    let nameXPosition = xPos;

    legend.append("text")
      .attr("class", "name") 
      .attr("text-anchor", "start")
      .attr("x", nameXPosition + 30)
      .attr("y", nameYPosition)
      .text(player.name.split(' ')[1])

    legend.append("line")          // attach a line
      .attr("class", "line " +  player.domId) 
      .attr("x1", nameXPosition)     
      .attr("y1", nameYPosition - 5)     
      .attr("x2", nameXPosition + 20)     
      .attr("y2", nameYPosition - 5); 
  }) 
}

function initRaceChart(playerArray){
  let config = getRaceChartConfig();
  let scales = getRaceChartScales(playerArray, config);
  drawRaceChartAxes(scales, config, playerArray);
  drawRaceChartLegend(config, playerArray);
  return {config, scales};
}

function getShotChartConfig(playerIndex) {
  let width = 75;
  let height = 85;
  let margin = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  }
  let bodyHeight = height - margin.top - margin.bottom
  let bodyWidth = width - margin.left - margin.right 

  let container = d3.selectAll(".shot-chart__goals").filter((d, i) => i === playerIndex);
  container
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", `0 0 ${width} ${height}`)

  return { width, height, margin, bodyHeight, bodyWidth, container }
}

function getShotChartScales(config) {
  let { bodyWidth, bodyHeight } = config;
  let xScale = d3.scaleLinear()
    .range([0, bodyWidth])
    .domain([25,100]) // Blueline starts at 25, 100 is end of o-zone

  let yScale = d3.scaleLinear()
    .range([0, bodyHeight])
    .domain([42.5, -42.5])
    
  return { xScale, yScale }
}

function drawShotChart(player, index){
  let config = getShotChartConfig(index);
  let scales = getShotChartScales(config);
  let {margin, container} = config; 
  let {xScale, yScale} = scales;

  //filter out empty netters from scoringPlays
  let goalArray = player.scoringPlays.map( play => {
    return play.coordinates;
  })

  //Create circles
  container.selectAll("circle")
     .data(player.scoringPlays)
     .enter()
     .append("circle")
     .attr("cx", (goal) => xScale(goal.coordinates.x))
     .attr("cy", (goal) => yScale(goal.coordinates.y))
     .attr("r", 1)
     .attr("class", "circle " + player.domId)
}

function load(){
  let players = playerData;
  console.log(players);
  addOverviewToDOM(players);
  initRaceChart(players);
  drawRaceChartLines(players);
  players.reverse().map((player, index)=> {
    drawShotChart(player, index);
  })
}

load();


