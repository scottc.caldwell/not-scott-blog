export { playerData }

const playerData = [
  {
    "name": "Alex Ovechkin",
    "id": 8471214,
    "team": "WSH",
    "teamLogo": "wsh-logo",
    "position": "LW",
    "domId": "ovechkin",
    "isWinner": true,
    "missedGames": [
      50
    ],

    "totalGoals": 48,
    "gamesPlayed": 68,
    "gamesRemaining": 13,
    "gameArray": [
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020002,
          "link": "/api/v1/game/2019020002/feed/live",
          "content": {
            "link": "/api/v1/game/2019020002/content"
          }
        },
        "index": 67
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020014,
          "link": "/api/v1/game/2019020014/feed/live",
          "content": {
            "link": "/api/v1/game/2019020014/content"
          }
        },
        "index": 66
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020023,
          "link": "/api/v1/game/2019020023/feed/live",
          "content": {
            "link": "/api/v1/game/2019020023/content"
          }
        },
        "index": 65
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020039,
          "link": "/api/v1/game/2019020039/feed/live",
          "content": {
            "link": "/api/v1/game/2019020039/content"
          }
        },
        "index": 64
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020052,
          "link": "/api/v1/game/2019020052/feed/live",
          "content": {
            "link": "/api/v1/game/2019020052/content"
          }
        },
        "index": 63
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020070,
          "link": "/api/v1/game/2019020070/feed/live",
          "content": {
            "link": "/api/v1/game/2019020070/content"
          }
        },
        "index": 62
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020083,
          "link": "/api/v1/game/2019020083/feed/live",
          "content": {
            "link": "/api/v1/game/2019020083/content"
          }
        },
        "index": 61
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020093,
          "link": "/api/v1/game/2019020093/feed/live",
          "content": {
            "link": "/api/v1/game/2019020093/content"
          }
        },
        "index": 60
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020109,
          "link": "/api/v1/game/2019020109/feed/live",
          "content": {
            "link": "/api/v1/game/2019020109/content"
          }
        },
        "index": 59
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020126,
          "link": "/api/v1/game/2019020126/feed/live",
          "content": {
            "link": "/api/v1/game/2019020126/content"
          }
        },
        "index": 58
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020142,
          "link": "/api/v1/game/2019020142/feed/live",
          "content": {
            "link": "/api/v1/game/2019020142/content"
          }
        },
        "index": 57
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020154,
          "link": "/api/v1/game/2019020154/feed/live",
          "content": {
            "link": "/api/v1/game/2019020154/content"
          }
        },
        "index": 56
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020160,
          "link": "/api/v1/game/2019020160/feed/live",
          "content": {
            "link": "/api/v1/game/2019020160/content"
          }
        },
        "index": 55
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020180,
          "link": "/api/v1/game/2019020180/feed/live",
          "content": {
            "link": "/api/v1/game/2019020180/content"
          }
        },
        "index": 54
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020198,
          "link": "/api/v1/game/2019020198/feed/live",
          "content": {
            "link": "/api/v1/game/2019020198/content"
          }
        },
        "index": 53
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020218,
          "link": "/api/v1/game/2019020218/feed/live",
          "content": {
            "link": "/api/v1/game/2019020218/content"
          }
        },
        "index": 52
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020238,
          "link": "/api/v1/game/2019020238/feed/live",
          "content": {
            "link": "/api/v1/game/2019020238/content"
          }
        },
        "index": 51
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020258,
          "link": "/api/v1/game/2019020258/feed/live",
          "content": {
            "link": "/api/v1/game/2019020258/content"
          }
        },
        "index": 50
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020270,
          "link": "/api/v1/game/2019020270/feed/live",
          "content": {
            "link": "/api/v1/game/2019020270/content"
          }
        },
        "index": 49
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020283,
          "link": "/api/v1/game/2019020283/feed/live",
          "content": {
            "link": "/api/v1/game/2019020283/content"
          }
        },
        "index": 48
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020296,
          "link": "/api/v1/game/2019020296/feed/live",
          "content": {
            "link": "/api/v1/game/2019020296/content"
          }
        },
        "index": 47
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020303,
          "link": "/api/v1/game/2019020303/feed/live",
          "content": {
            "link": "/api/v1/game/2019020303/content"
          }
        },
        "index": 46
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020316,
          "link": "/api/v1/game/2019020316/feed/live",
          "content": {
            "link": "/api/v1/game/2019020316/content"
          }
        },
        "index": 45
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020332,
          "link": "/api/v1/game/2019020332/feed/live",
          "content": {
            "link": "/api/v1/game/2019020332/content"
          }
        },
        "index": 44
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020348,
          "link": "/api/v1/game/2019020348/feed/live",
          "content": {
            "link": "/api/v1/game/2019020348/content"
          }
        },
        "index": 43
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020383,
          "link": "/api/v1/game/2019020383/feed/live",
          "content": {
            "link": "/api/v1/game/2019020383/content"
          }
        },
        "index": 42
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020398,
          "link": "/api/v1/game/2019020398/feed/live",
          "content": {
            "link": "/api/v1/game/2019020398/content"
          }
        },
        "index": 41
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020406,
          "link": "/api/v1/game/2019020406/feed/live",
          "content": {
            "link": "/api/v1/game/2019020406/content"
          }
        },
        "index": 40
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020433,
          "link": "/api/v1/game/2019020433/feed/live",
          "content": {
            "link": "/api/v1/game/2019020433/content"
          }
        },
        "index": 39
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020437,
          "link": "/api/v1/game/2019020437/feed/live",
          "content": {
            "link": "/api/v1/game/2019020437/content"
          }
        },
        "index": 38
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020451,
          "link": "/api/v1/game/2019020451/feed/live",
          "content": {
            "link": "/api/v1/game/2019020451/content"
          }
        },
        "index": 37
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020469,
          "link": "/api/v1/game/2019020469/feed/live",
          "content": {
            "link": "/api/v1/game/2019020469/content"
          }
        },
        "index": 36
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020484,
          "link": "/api/v1/game/2019020484/feed/live",
          "content": {
            "link": "/api/v1/game/2019020484/content"
          }
        },
        "index": 35
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020507,
          "link": "/api/v1/game/2019020507/feed/live",
          "content": {
            "link": "/api/v1/game/2019020507/content"
          }
        },
        "index": 34
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020521,
          "link": "/api/v1/game/2019020521/feed/live",
          "content": {
            "link": "/api/v1/game/2019020521/content"
          }
        },
        "index": 33
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020549,
          "link": "/api/v1/game/2019020549/feed/live",
          "content": {
            "link": "/api/v1/game/2019020549/content"
          }
        },
        "index": 32
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020558,
          "link": "/api/v1/game/2019020558/feed/live",
          "content": {
            "link": "/api/v1/game/2019020558/content"
          }
        },
        "index": 31
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020571,
          "link": "/api/v1/game/2019020571/feed/live",
          "content": {
            "link": "/api/v1/game/2019020571/content"
          }
        },
        "index": 30
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020585,
          "link": "/api/v1/game/2019020585/feed/live",
          "content": {
            "link": "/api/v1/game/2019020585/content"
          }
        },
        "index": 29
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020597,
          "link": "/api/v1/game/2019020597/feed/live",
          "content": {
            "link": "/api/v1/game/2019020597/content"
          }
        },
        "index": 28
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020614,
          "link": "/api/v1/game/2019020614/feed/live",
          "content": {
            "link": "/api/v1/game/2019020614/content"
          }
        },
        "index": 27
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020639,
          "link": "/api/v1/game/2019020639/feed/live",
          "content": {
            "link": "/api/v1/game/2019020639/content"
          }
        },
        "index": 26
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020653,
          "link": "/api/v1/game/2019020653/feed/live",
          "content": {
            "link": "/api/v1/game/2019020653/content"
          }
        },
        "index": 25
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020667,
          "link": "/api/v1/game/2019020667/feed/live",
          "content": {
            "link": "/api/v1/game/2019020667/content"
          }
        },
        "index": 24
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020676,
          "link": "/api/v1/game/2019020676/feed/live",
          "content": {
            "link": "/api/v1/game/2019020676/content"
          }
        },
        "index": 23
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020696,
          "link": "/api/v1/game/2019020696/feed/live",
          "content": {
            "link": "/api/v1/game/2019020696/content"
          }
        },
        "index": 22
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020712,
          "link": "/api/v1/game/2019020712/feed/live",
          "content": {
            "link": "/api/v1/game/2019020712/content"
          }
        },
        "index": 21
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020732,
          "link": "/api/v1/game/2019020732/feed/live",
          "content": {
            "link": "/api/v1/game/2019020732/content"
          }
        },
        "index": 20
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020743,
          "link": "/api/v1/game/2019020743/feed/live",
          "content": {
            "link": "/api/v1/game/2019020743/content"
          }
        },
        "index": 19
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020776,
          "link": "/api/v1/game/2019020776/feed/live",
          "content": {
            "link": "/api/v1/game/2019020776/content"
          }
        },
        "index": 18
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020787,
          "link": "/api/v1/game/2019020787/feed/live",
          "content": {
            "link": "/api/v1/game/2019020787/content"
          }
        },
        "index": 17
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020806,
          "link": "/api/v1/game/2019020806/feed/live",
          "content": {
            "link": "/api/v1/game/2019020806/content"
          }
        },
        "index": 16
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020817,
          "link": "/api/v1/game/2019020817/feed/live",
          "content": {
            "link": "/api/v1/game/2019020817/content"
          }
        },
        "index": 15
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020849,
          "link": "/api/v1/game/2019020849/feed/live",
          "content": {
            "link": "/api/v1/game/2019020849/content"
          }
        },
        "index": 14
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020862,
          "link": "/api/v1/game/2019020862/feed/live",
          "content": {
            "link": "/api/v1/game/2019020862/content"
          }
        },
        "index": 13
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020888,
          "link": "/api/v1/game/2019020888/feed/live",
          "content": {
            "link": "/api/v1/game/2019020888/content"
          }
        },
        "index": 12
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020904,
          "link": "/api/v1/game/2019020904/feed/live",
          "content": {
            "link": "/api/v1/game/2019020904/content"
          }
        },
        "index": 11
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020918,
          "link": "/api/v1/game/2019020918/feed/live",
          "content": {
            "link": "/api/v1/game/2019020918/content"
          }
        },
        "index": 10
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020935,
          "link": "/api/v1/game/2019020935/feed/live",
          "content": {
            "link": "/api/v1/game/2019020935/content"
          }
        },
        "index": 9
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020948,
          "link": "/api/v1/game/2019020948/feed/live",
          "content": {
            "link": "/api/v1/game/2019020948/content"
          }
        },
        "index": 8
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020959,
          "link": "/api/v1/game/2019020959/feed/live",
          "content": {
            "link": "/api/v1/game/2019020959/content"
          }
        },
        "index": 7
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020973,
          "link": "/api/v1/game/2019020973/feed/live",
          "content": {
            "link": "/api/v1/game/2019020973/content"
          }
        },
        "index": 6
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020992,
          "link": "/api/v1/game/2019020992/feed/live",
          "content": {
            "link": "/api/v1/game/2019020992/content"
          }
        },
        "index": 5
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019021014,
          "link": "/api/v1/game/2019021014/feed/live",
          "content": {
            "link": "/api/v1/game/2019021014/content"
          }
        },
        "index": 4
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021029,
          "link": "/api/v1/game/2019021029/feed/live",
          "content": {
            "link": "/api/v1/game/2019021029/content"
          }
        },
        "index": 3
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019021036,
          "link": "/api/v1/game/2019021036/feed/live",
          "content": {
            "link": "/api/v1/game/2019021036/content"
          }
        },
        "index": 2
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021050,
          "link": "/api/v1/game/2019021050/feed/live",
          "content": {
            "link": "/api/v1/game/2019021050/content"
          }
        },
        "index": 1
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021066,
          "link": "/api/v1/game/2019021066/feed/live",
          "content": {
            "link": "/api/v1/game/2019021066/content"
          }
        },
        "index": 0
      }
    ],
    "sumArray": [
      0,
      1,
      1,
      1,
      1,
      3,
      4,
      5,
      5,
      5,
      6,
      7,
      9,
      9,
      11,
      11,
      11,
      13,
      13,
      13,
      13,
      14,
      14,
      15,
      15,
      15,
      16,
      17,
      20,
      20,
      20,
      20,
      21,
      21,
      21,
      21,
      22,
      22,
      23,
      23,
      24,
      24,
      24,
      24,
      26,
      26,
      26,
      28,
      31,
      34,
      35,
      35,
      37,
      37,
      40,
      40,
      40,
      40,
      40,
      40,
      41,
      42,
      42,
      43,
      43,
      45,
      45,
      47,
      47,
      48
    ],
    "scoringPlays": [
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8474189,
              "fullName": "Lars Eller",
              "link": "/api/v1/people/8474189"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8476412,
              "fullName": "Jordan Binnington",
              "link": "/api/v1/people/8476412"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 57,
          "y": -18
        },
        "result": {
          "event": "Goal",
          "eventCode": "STL130",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (1) Snap Shot, assists: John Carlson (1), Lars Eller (1)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 53,
          "y": 22
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH279",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (2) Slap Shot, assists: John Carlson (6), Evgeny Kuznetsov (1)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 60,
          "y": 20
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH464",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (3) Slap Shot, assists: John Carlson (7), Nicklas Backstrom (3)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          }
        ],
        "coordinates": {
          "x": 4,
          "y": -34
        },
        "result": {
          "event": "Goal",
          "eventCode": "DAL783",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (4) Backhand, assists: Tom Wilson (2)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8475831,
              "fullName": "Philipp Grubauer",
              "link": "/api/v1/people/8475831"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 34,
          "y": -7
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH802",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (5) Slap Shot, assists: Evgeny Kuznetsov (3), John Carlson (9)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8479482,
              "fullName": "Michal Kempny",
              "link": "/api/v1/people/8479482"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8470645,
              "fullName": "Corey Crawford",
              "link": "/api/v1/people/8470645"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 61,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "CHI444",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (6) Wrist Shot, assists: John Carlson (15), Michal Kempny (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8475462,
              "fullName": "Radko Gudas",
              "link": "/api/v1/people/8475462"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8475660,
              "fullName": "Cam Talbot",
              "link": "/api/v1/people/8475660"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 63,
          "y": 19
        },
        "result": {
          "event": "Goal",
          "eventCode": "CGY294",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (7) Snap Shot, assists: Nicklas Backstrom (7), Radko Gudas (3)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8478399,
              "fullName": "Jonas Siegenthaler",
              "link": "/api/v1/people/8478399"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8475156,
              "fullName": "Mikko Koskinen",
              "link": "/api/v1/people/8475156"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": 11
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM383",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (8) Tip-In, assists: Jonas Siegenthaler (2), John Carlson (16)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8477944,
              "fullName": "Jakub Vrana",
              "link": "/api/v1/people/8477944"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8475156,
              "fullName": "Mikko Koskinen",
              "link": "/api/v1/people/8475156"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": 13
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM506",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (9) Snap Shot, assists: Tom Wilson (3), Jakub Vrana (3)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8471698,
              "fullName": "T.J. Oshie",
              "link": "/api/v1/people/8471698"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8475883,
              "fullName": "Frederik Andersen",
              "link": "/api/v1/people/8475883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 8
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR610",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (10) Snap Shot, assists: T.J. Oshie (4), Nicklas Backstrom (10)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8477944,
              "fullName": "Jakub Vrana",
              "link": "/api/v1/people/8477944"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8475883,
              "fullName": "Frederik Andersen",
              "link": "/api/v1/people/8475883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 57,
          "y": 23
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR883",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (11) Slap Shot, assists: Dmitry Orlov (5), Jakub Vrana (5)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 72,
          "y": 19
        },
        "result": {
          "event": "Goal",
          "eventCode": "FLA274",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (12) Wrist Shot, assists: Evgeny Kuznetsov (7), John Carlson (17)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": 30
        },
        "result": {
          "event": "Goal",
          "eventCode": "FLA298",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (13) Tip-In, assists: Dmitry Orlov (6), Nicklas Backstrom (11)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 53,
          "y": 24
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH614",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (14) Snap Shot, assists: John Carlson (23), Nicklas Backstrom (12)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 70,
          "y": 22
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH241",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (15) Wrist Shot, assists: Evgeny Kuznetsov (12), John Carlson (25)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH206",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (16) Wrist Shot, assists: Evgeny Kuznetsov (13)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8476329,
              "fullName": "Travis Boyd",
              "link": "/api/v1/people/8476329"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8476883,
              "fullName": "Andrei Vasilevskiy",
              "link": "/api/v1/people/8476883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 55,
          "y": 28
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH629",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (17) Slap Shot, assists: Dmitry Orlov (8), Travis Boyd (6)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8473541,
              "fullName": "Jonathan Bernier",
              "link": "/api/v1/people/8473541"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": 15
        },
        "result": {
          "event": "Goal",
          "eventCode": "DET282",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (18) Wrist Shot, assists: John Carlson (29), Evgeny Kuznetsov (16)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8471698,
              "fullName": "T.J. Oshie",
              "link": "/api/v1/people/8471698"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8475462,
              "fullName": "Radko Gudas",
              "link": "/api/v1/people/8475462"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "DET761",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (19) Snap Shot, assists: T.J. Oshie (9), Radko Gudas (7)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 20
          }
        ],
        "coordinates": {
          "x": 80,
          "y": 0
        },
        "result": {
          "event": "Goal",
          "eventCode": "DET765",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (20) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8476914,
              "fullName": "Joonas Korpisalo",
              "link": "/api/v1/people/8476914"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": -5
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH603",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (21) Backhand, assists: Dmitry Orlov (11), Nicklas Backstrom (15)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 49,
          "y": -25
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD201",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (22) Snap Shot, assists: Nicklas Backstrom (18), Tom Wilson (11)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8471695,
              "fullName": "Tuukka Rask",
              "link": "/api/v1/people/8471695"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 63,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS453",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (23) Wrist Shot, assists: Tom Wilson (14), John Carlson (35)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8474189,
              "fullName": "Lars Eller",
              "link": "/api/v1/people/8474189"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475852,
              "fullName": "Petr Mrazek",
              "link": "/api/v1/people/8475852"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 56,
          "y": 24
        },
        "result": {
          "event": "Goal",
          "eventCode": "CAR236",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (24) Slap Shot, assists: Dmitry Orlov (15), Lars Eller (13)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 61,
          "y": -26
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH466",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (25) Snap Shot, assists: Dmitry Orlov (18), Tom Wilson (15)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8477405,
              "fullName": "Marcus Hogberg",
              "link": "/api/v1/people/8477405"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 26
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH678",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (26) Slap Shot, assists: John Carlson (41), Tom Wilson (16)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8475852,
              "fullName": "Petr Mrazek",
              "link": "/api/v1/people/8475852"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH41",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (27) Wrist Shot, assists: Tom Wilson (18)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8477944,
              "fullName": "Jakub Vrana",
              "link": "/api/v1/people/8477944"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8475852,
              "fullName": "Petr Mrazek",
              "link": "/api/v1/people/8475852"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 53,
          "y": 22
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH210",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (28) Slap Shot, assists: Jakub Vrana (18), John Carlson (43)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 44
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8475839,
              "fullName": "Louis Domingue",
              "link": "/api/v1/people/8475839"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 52,
          "y": 21
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH262",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (29) Slap Shot, assists: John Carlson (44), Nicklas Backstrom (27)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8475839,
              "fullName": "Louis Domingue",
              "link": "/api/v1/people/8475839"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 59,
          "y": 16
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH271",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (30) Snap Shot, assists: Nicklas Backstrom (28)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 45
          },
          {
            "player": {
              "id": 8471239,
              "fullName": "Cory Schneider",
              "link": "/api/v1/people/8471239"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH755",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (31) Wrist Shot, assists: Tom Wilson (19), John Carlson (45)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8473575,
              "fullName": "Semyon Varlamov",
              "link": "/api/v1/people/8473575"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": -5
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYI83",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (32) Wrist Shot, assists: Nicklas Backstrom (29)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 46
          },
          {
            "player": {
              "id": 8473575,
              "fullName": "Semyon Varlamov",
              "link": "/api/v1/people/8473575"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 21
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYI485",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (33) Wrist Shot, assists: John Carlson (46)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8471698,
              "fullName": "T.J. Oshie",
              "link": "/api/v1/people/8471698"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          }
        ],
        "coordinates": {
          "x": 19,
          "y": 15
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYI657",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (34) Wrist Shot, assists: T.J. Oshie (16)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8477424,
              "fullName": "Juuse Saros",
              "link": "/api/v1/people/8477424"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -3
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH207",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (35) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8476880,
              "fullName": "Tom Wilson",
              "link": "/api/v1/people/8476880"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8477405,
              "fullName": "Marcus Hogberg",
              "link": "/api/v1/people/8477405"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 67,
          "y": -11
        },
        "result": {
          "event": "Goal",
          "eventCode": "OTT468",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (36) Wrist Shot, assists: Nicklas Backstrom (30), Tom Wilson (21)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 49
          }
        ],
        "coordinates": {
          "x": 48,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "OTT716",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (37) Wrist Shot, assists: John Carlson (49)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 38
          },
          {
            "player": {
              "id": 8471698,
              "fullName": "T.J. Oshie",
              "link": "/api/v1/people/8471698"
            },
            "playerType": "Assist",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8471734,
              "fullName": "Jonathan Quick",
              "link": "/api/v1/people/8471734"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH691",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (38) Wrist Shot, assists: T.J. Oshie (19)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 50
          },
          {
            "player": {
              "id": 8471734,
              "fullName": "Jonathan Quick",
              "link": "/api/v1/people/8471734"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH695",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (39) Wrist Shot, assists: John Carlson (50)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 51
          }
        ],
        "coordinates": {
          "x": 49,
          "y": 0
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH705",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (40) Wrist Shot, assists: John Carlson (51)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 52,
          "y": 20
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH30",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (41) Wrist Shot, assists: Nicklas Backstrom (35)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 42
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8475324,
              "fullName": "Nick Jensen",
              "link": "/api/v1/people/8475324"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 64,
          "y": -20
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD651",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (42) Slap Shot, assists: Evgeny Kuznetsov (29), Nick Jensen (6)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8475200,
              "fullName": "Dmitry Orlov",
              "link": "/api/v1/people/8475200"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8476316,
              "fullName": "Laurent Brossoit",
              "link": "/api/v1/people/8476316"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -3
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH18",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (43) Snap Shot, assists: Evgeny Kuznetsov (30), Dmitry Orlov (22)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 0
          },
          {
            "player": {
              "id": 8476316,
              "fullName": "Laurent Brossoit",
              "link": "/api/v1/people/8476316"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH727",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin - Backhand",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 44
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 58
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8471774,
              "fullName": "Alex Stalock",
              "link": "/api/v1/people/8471774"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 31,
          "y": 21
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN65",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (44) Slap Shot, assists: John Carlson (58), Nicklas Backstrom (40)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 45
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8469454,
              "fullName": "Ilya Kovalchuk",
              "link": "/api/v1/people/8469454"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8471774,
              "fullName": "Alex Stalock",
              "link": "/api/v1/people/8471774"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 66,
          "y": 22
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN69",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (45) Slap Shot, assists: Evgeny Kuznetsov (31), Ilya Kovalchuk (14)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 46
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 59
          },
          {
            "player": {
              "id": 8473563,
              "fullName": "Nicklas Backstrom",
              "link": "/api/v1/people/8473563"
            },
            "playerType": "Assist",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8480382,
              "fullName": "Alexandar Georgiev",
              "link": "/api/v1/people/8480382"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 57,
          "y": 16
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR605",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (46) Snap Shot, assists: John Carlson (59), Nicklas Backstrom (41)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 47
          },
          {
            "player": {
              "id": 8469454,
              "fullName": "Ilya Kovalchuk",
              "link": "/api/v1/people/8469454"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8474590,
              "fullName": "John Carlson",
              "link": "/api/v1/people/8474590"
            },
            "playerType": "Assist",
            "seasonTotal": 60
          },
          {
            "player": {
              "id": 8480382,
              "fullName": "Alexandar Georgiev",
              "link": "/api/v1/people/8480382"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": 9
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR637",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (47) Snap Shot, assists: Ilya Kovalchuk (16), John Carlson (60)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8471214,
              "fullName": "Alex Ovechkin",
              "link": "/api/v1/people/8471214"
            },
            "playerType": "Scorer",
            "seasonTotal": 48
          },
          {
            "player": {
              "id": 8475744,
              "fullName": "Evgeny Kuznetsov",
              "link": "/api/v1/people/8475744"
            },
            "playerType": "Assist",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8476999,
              "fullName": "Linus Ullmark",
              "link": "/api/v1/people/8476999"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "BUF512",
          "eventTypeId": "GOAL",
          "description": "Alex Ovechkin (48) Wrist Shot, assists: Evgeny Kuznetsov (33)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      }
    ]
  },
  {
    "name": "David Pastrnak",
    "id": 8477956,
    "team": "BOS",
    "teamLogo": "bos-logo",
    "position": "RW",
    "domId": "pastrnak",
    "isWinner": true,
    "missedGames": [],
    "totalGoals": 48,
    "gamesPlayed": 70,
    "gamesRemaining": 12,
    "gameArray": [
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020010,
          "link": "/api/v1/game/2019020010/feed/live",
          "content": {
            "link": "/api/v1/game/2019020010/content"
          }
        },
        "index": 69
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020027,
          "link": "/api/v1/game/2019020027/feed/live",
          "content": {
            "link": "/api/v1/game/2019020027/content"
          }
        },
        "index": 68
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020043,
          "link": "/api/v1/game/2019020043/feed/live",
          "content": {
            "link": "/api/v1/game/2019020043/content"
          }
        },
        "index": 67
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020056,
          "link": "/api/v1/game/2019020056/feed/live",
          "content": {
            "link": "/api/v1/game/2019020056/content"
          }
        },
        "index": 66
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020064,
          "link": "/api/v1/game/2019020064/feed/live",
          "content": {
            "link": "/api/v1/game/2019020064/content"
          }
        },
        "index": 65
      },
      {
        "goals": 4,
        "game": {
          "gamePk": 2019020078,
          "link": "/api/v1/game/2019020078/feed/live",
          "content": {
            "link": "/api/v1/game/2019020078/content"
          }
        },
        "index": 64
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020098,
          "link": "/api/v1/game/2019020098/feed/live",
          "content": {
            "link": "/api/v1/game/2019020098/content"
          }
        },
        "index": 63
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020115,
          "link": "/api/v1/game/2019020115/feed/live",
          "content": {
            "link": "/api/v1/game/2019020115/content"
          }
        },
        "index": 62
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020133,
          "link": "/api/v1/game/2019020133/feed/live",
          "content": {
            "link": "/api/v1/game/2019020133/content"
          }
        },
        "index": 61
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020162,
          "link": "/api/v1/game/2019020162/feed/live",
          "content": {
            "link": "/api/v1/game/2019020162/content"
          }
        },
        "index": 60
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020174,
          "link": "/api/v1/game/2019020174/feed/live",
          "content": {
            "link": "/api/v1/game/2019020174/content"
          }
        },
        "index": 59
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020179,
          "link": "/api/v1/game/2019020179/feed/live",
          "content": {
            "link": "/api/v1/game/2019020179/content"
          }
        },
        "index": 58
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020206,
          "link": "/api/v1/game/2019020206/feed/live",
          "content": {
            "link": "/api/v1/game/2019020206/content"
          }
        },
        "index": 57
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020220,
          "link": "/api/v1/game/2019020220/feed/live",
          "content": {
            "link": "/api/v1/game/2019020220/content"
          }
        },
        "index": 56
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020224,
          "link": "/api/v1/game/2019020224/feed/live",
          "content": {
            "link": "/api/v1/game/2019020224/content"
          }
        },
        "index": 55
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020248,
          "link": "/api/v1/game/2019020248/feed/live",
          "content": {
            "link": "/api/v1/game/2019020248/content"
          }
        },
        "index": 54
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020267,
          "link": "/api/v1/game/2019020267/feed/live",
          "content": {
            "link": "/api/v1/game/2019020267/content"
          }
        },
        "index": 53
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020272,
          "link": "/api/v1/game/2019020272/feed/live",
          "content": {
            "link": "/api/v1/game/2019020272/content"
          }
        },
        "index": 52
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020294,
          "link": "/api/v1/game/2019020294/feed/live",
          "content": {
            "link": "/api/v1/game/2019020294/content"
          }
        },
        "index": 51
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020303,
          "link": "/api/v1/game/2019020303/feed/live",
          "content": {
            "link": "/api/v1/game/2019020303/content"
          }
        },
        "index": 50
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020320,
          "link": "/api/v1/game/2019020320/feed/live",
          "content": {
            "link": "/api/v1/game/2019020320/content"
          }
        },
        "index": 49
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020333,
          "link": "/api/v1/game/2019020333/feed/live",
          "content": {
            "link": "/api/v1/game/2019020333/content"
          }
        },
        "index": 48
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020351,
          "link": "/api/v1/game/2019020351/feed/live",
          "content": {
            "link": "/api/v1/game/2019020351/content"
          }
        },
        "index": 47
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020374,
          "link": "/api/v1/game/2019020374/feed/live",
          "content": {
            "link": "/api/v1/game/2019020374/content"
          }
        },
        "index": 46
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020378,
          "link": "/api/v1/game/2019020378/feed/live",
          "content": {
            "link": "/api/v1/game/2019020378/content"
          }
        },
        "index": 45
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020391,
          "link": "/api/v1/game/2019020391/feed/live",
          "content": {
            "link": "/api/v1/game/2019020391/content"
          }
        },
        "index": 44
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020417,
          "link": "/api/v1/game/2019020417/feed/live",
          "content": {
            "link": "/api/v1/game/2019020417/content"
          }
        },
        "index": 43
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020424,
          "link": "/api/v1/game/2019020424/feed/live",
          "content": {
            "link": "/api/v1/game/2019020424/content"
          }
        },
        "index": 42
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020438,
          "link": "/api/v1/game/2019020438/feed/live",
          "content": {
            "link": "/api/v1/game/2019020438/content"
          }
        },
        "index": 41
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020454,
          "link": "/api/v1/game/2019020454/feed/live",
          "content": {
            "link": "/api/v1/game/2019020454/content"
          }
        },
        "index": 40
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020470,
          "link": "/api/v1/game/2019020470/feed/live",
          "content": {
            "link": "/api/v1/game/2019020470/content"
          }
        },
        "index": 39
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020484,
          "link": "/api/v1/game/2019020484/feed/live",
          "content": {
            "link": "/api/v1/game/2019020484/content"
          }
        },
        "index": 38
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020488,
          "link": "/api/v1/game/2019020488/feed/live",
          "content": {
            "link": "/api/v1/game/2019020488/content"
          }
        },
        "index": 37
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020508,
          "link": "/api/v1/game/2019020508/feed/live",
          "content": {
            "link": "/api/v1/game/2019020508/content"
          }
        },
        "index": 36
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020524,
          "link": "/api/v1/game/2019020524/feed/live",
          "content": {
            "link": "/api/v1/game/2019020524/content"
          }
        },
        "index": 35
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020538,
          "link": "/api/v1/game/2019020538/feed/live",
          "content": {
            "link": "/api/v1/game/2019020538/content"
          }
        },
        "index": 34
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020555,
          "link": "/api/v1/game/2019020555/feed/live",
          "content": {
            "link": "/api/v1/game/2019020555/content"
          }
        },
        "index": 33
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020571,
          "link": "/api/v1/game/2019020571/feed/live",
          "content": {
            "link": "/api/v1/game/2019020571/content"
          }
        },
        "index": 32
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020582,
          "link": "/api/v1/game/2019020582/feed/live",
          "content": {
            "link": "/api/v1/game/2019020582/content"
          }
        },
        "index": 31
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020606,
          "link": "/api/v1/game/2019020606/feed/live",
          "content": {
            "link": "/api/v1/game/2019020606/content"
          }
        },
        "index": 30
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020613,
          "link": "/api/v1/game/2019020613/feed/live",
          "content": {
            "link": "/api/v1/game/2019020613/content"
          }
        },
        "index": 29
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020627,
          "link": "/api/v1/game/2019020627/feed/live",
          "content": {
            "link": "/api/v1/game/2019020627/content"
          }
        },
        "index": 28
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020641,
          "link": "/api/v1/game/2019020641/feed/live",
          "content": {
            "link": "/api/v1/game/2019020641/content"
          }
        },
        "index": 27
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020671,
          "link": "/api/v1/game/2019020671/feed/live",
          "content": {
            "link": "/api/v1/game/2019020671/content"
          }
        },
        "index": 26
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020678,
          "link": "/api/v1/game/2019020678/feed/live",
          "content": {
            "link": "/api/v1/game/2019020678/content"
          }
        },
        "index": 25
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020694,
          "link": "/api/v1/game/2019020694/feed/live",
          "content": {
            "link": "/api/v1/game/2019020694/content"
          }
        },
        "index": 24
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020711,
          "link": "/api/v1/game/2019020711/feed/live",
          "content": {
            "link": "/api/v1/game/2019020711/content"
          }
        },
        "index": 23
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020719,
          "link": "/api/v1/game/2019020719/feed/live",
          "content": {
            "link": "/api/v1/game/2019020719/content"
          }
        },
        "index": 22
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020727,
          "link": "/api/v1/game/2019020727/feed/live",
          "content": {
            "link": "/api/v1/game/2019020727/content"
          }
        },
        "index": 21
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020755,
          "link": "/api/v1/game/2019020755/feed/live",
          "content": {
            "link": "/api/v1/game/2019020755/content"
          }
        },
        "index": 20
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020761,
          "link": "/api/v1/game/2019020761/feed/live",
          "content": {
            "link": "/api/v1/game/2019020761/content"
          }
        },
        "index": 19
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020789,
          "link": "/api/v1/game/2019020789/feed/live",
          "content": {
            "link": "/api/v1/game/2019020789/content"
          }
        },
        "index": 18
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020801,
          "link": "/api/v1/game/2019020801/feed/live",
          "content": {
            "link": "/api/v1/game/2019020801/content"
          }
        },
        "index": 17
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020812,
          "link": "/api/v1/game/2019020812/feed/live",
          "content": {
            "link": "/api/v1/game/2019020812/content"
          }
        },
        "index": 16
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020826,
          "link": "/api/v1/game/2019020826/feed/live",
          "content": {
            "link": "/api/v1/game/2019020826/content"
          }
        },
        "index": 15
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020844,
          "link": "/api/v1/game/2019020844/feed/live",
          "content": {
            "link": "/api/v1/game/2019020844/content"
          }
        },
        "index": 14
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020855,
          "link": "/api/v1/game/2019020855/feed/live",
          "content": {
            "link": "/api/v1/game/2019020855/content"
          }
        },
        "index": 13
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020877,
          "link": "/api/v1/game/2019020877/feed/live",
          "content": {
            "link": "/api/v1/game/2019020877/content"
          }
        },
        "index": 12
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020895,
          "link": "/api/v1/game/2019020895/feed/live",
          "content": {
            "link": "/api/v1/game/2019020895/content"
          }
        },
        "index": 11
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020907,
          "link": "/api/v1/game/2019020907/feed/live",
          "content": {
            "link": "/api/v1/game/2019020907/content"
          }
        },
        "index": 10
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020929,
          "link": "/api/v1/game/2019020929/feed/live",
          "content": {
            "link": "/api/v1/game/2019020929/content"
          }
        },
        "index": 9
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020945,
          "link": "/api/v1/game/2019020945/feed/live",
          "content": {
            "link": "/api/v1/game/2019020945/content"
          }
        },
        "index": 8
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020956,
          "link": "/api/v1/game/2019020956/feed/live",
          "content": {
            "link": "/api/v1/game/2019020956/content"
          }
        },
        "index": 7
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020968,
          "link": "/api/v1/game/2019020968/feed/live",
          "content": {
            "link": "/api/v1/game/2019020968/content"
          }
        },
        "index": 6
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020984,
          "link": "/api/v1/game/2019020984/feed/live",
          "content": {
            "link": "/api/v1/game/2019020984/content"
          }
        },
        "index": 5
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020999,
          "link": "/api/v1/game/2019020999/feed/live",
          "content": {
            "link": "/api/v1/game/2019020999/content"
          }
        },
        "index": 4
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021019,
          "link": "/api/v1/game/2019021019/feed/live",
          "content": {
            "link": "/api/v1/game/2019021019/content"
          }
        },
        "index": 3
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021035,
          "link": "/api/v1/game/2019021035/feed/live",
          "content": {
            "link": "/api/v1/game/2019021035/content"
          }
        },
        "index": 2
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021053,
          "link": "/api/v1/game/2019021053/feed/live",
          "content": {
            "link": "/api/v1/game/2019021053/content"
          }
        },
        "index": 1
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021073,
          "link": "/api/v1/game/2019021073/feed/live",
          "content": {
            "link": "/api/v1/game/2019021073/content"
          }
        },
        "index": 0
      }
    ],
    "sumArray": [
      0,
      0,
      0,
      1,
      2,
      2,
      6,
      8,
      9,
      10,
      11,
      11,
      12,
      13,
      14,
      15,
      15,
      15,
      16,
      16,
      17,
      19,
      20,
      20,
      23,
      23,
      24,
      25,
      25,
      25,
      25,
      25,
      26,
      26,
      28,
      28,
      28,
      28,
      28,
      28,
      29,
      29,
      30,
      31,
      32,
      35,
      35,
      36,
      36,
      36,
      37,
      37,
      37,
      38,
      38,
      38,
      38,
      38,
      41,
      42,
      42,
      43,
      43,
      45,
      45,
      46,
      47,
      47,
      47,
      48,
      48
    ],
    "scoringPlays": [
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8470594,
              "fullName": "Marc-Andre Fleury",
              "link": "/api/v1/people/8470594"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "VGK133",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (1) Wrist Shot, assists: Brad Marchand (1), Patrice Bergeron (2)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8475831,
              "fullName": "Philipp Grubauer",
              "link": "/api/v1/people/8475831"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "COL35",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (2) Wrist Shot, assists: Brad Marchand (2), David Krejci (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 66,
          "y": 19
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS117",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (3) Slap Shot, assists: Patrice Bergeron (4), Torey Krug (2)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 64,
          "y": -11
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS349",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (4) Wrist Shot, assists: Brad Marchand (4)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": 12
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS531",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (5) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": -2
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS544",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (6) Tip-In, assists: Brad Marchand (5), Torey Krug (3)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476883,
              "fullName": "Andrei Vasilevskiy",
              "link": "/api/v1/people/8476883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS90",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (7) Slap Shot, assists: Patrice Bergeron (5), Torey Krug (4)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476883,
              "fullName": "Andrei Vasilevskiy",
              "link": "/api/v1/people/8476883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS642",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (8) Wrist Shot, assists: Brad Marchand (7), Torey Krug (5)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8475883,
              "fullName": "Frederik Andersen",
              "link": "/api/v1/people/8475883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 62,
          "y": 29
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR731",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (9) Slap Shot, assists: Brad Marchand (8), Patrice Bergeron (6)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8474636,
              "fullName": "Michael Hutchinson",
              "link": "/api/v1/people/8474636"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS252",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (10) Wrist Shot, assists: Brad Marchand (9), Torey Krug (6)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8476412,
              "fullName": "Jordan Binnington",
              "link": "/api/v1/people/8476412"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 59,
          "y": 19
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS301",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (11) Slap Shot, assists: Torey Krug (7), Brad Marchand (10)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8474889,
              "fullName": "Martin Jones",
              "link": "/api/v1/people/8474889"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 75,
          "y": 15
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS123",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (12) Wrist Shot, assists: Patrice Bergeron (7)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS109",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (13) Wrist Shot, assists: Torey Krug (9)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8478443,
              "fullName": "Brandon Carlo",
              "link": "/api/v1/people/8478443"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476899,
              "fullName": "Matt Murray",
              "link": "/api/v1/people/8476899"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 47,
          "y": 29
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS223",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (14) Wrist Shot, assists: Brad Marchand (16), Brandon Carlo (4)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 63,
          "y": 30
        },
        "result": {
          "event": "Goal",
          "eventCode": "MTL143",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (15) Slap Shot, assists: Torey Krug (10), Patrice Bergeron (9)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 63,
          "y": 14
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS296",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (16) Wrist Shot, assists: Patrice Bergeron (10)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8479325,
              "fullName": "Charlie McAvoy",
              "link": "/api/v1/people/8479325"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8474651,
              "fullName": "Braden Holtby",
              "link": "/api/v1/people/8474651"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 84,
          "y": -12
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS235",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (17) Snap Shot, assists: Charlie McAvoy (6), David Krejci (9)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": 16
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD36",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (18) Slap Shot, assists: Brad Marchand (21), David Krejci (11)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8475745,
              "fullName": "Charlie Coyle",
              "link": "/api/v1/people/8475745"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 43,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD576",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (19) Slap Shot, assists: Brad Marchand (22), Charlie Coyle (7)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8478046,
              "fullName": "Danton Heinen",
              "link": "/api/v1/people/8478046"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8476999,
              "fullName": "Linus Ullmark",
              "link": "/api/v1/people/8476999"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS439",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (20) Wrist Shot, assists: Danton Heinen (7), Patrice Bergeron (12)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475745,
              "fullName": "Charlie Coyle",
              "link": "/api/v1/people/8475745"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 21
        },
        "result": {
          "event": "Goal",
          "eventCode": "MTL96",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (21) Snap Shot, assists: Charlie Coyle (8), Brad Marchand (24)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8465009,
              "fullName": "Zdeno Chara",
              "link": "/api/v1/people/8465009"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 60,
          "y": 13
        },
        "result": {
          "event": "Goal",
          "eventCode": "MTL260",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (22) Wrist Shot, assists: Brad Marchand (25), Zdeno Chara (7)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8478443,
              "fullName": "Brandon Carlo",
              "link": "/api/v1/people/8478443"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8476234,
              "fullName": "Keith Kinkaid",
              "link": "/api/v1/people/8476234"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "MTL287",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (23) Tip-In, assists: Brandon Carlo (6), David Krejci (13)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8478498,
              "fullName": "Jake DeBrusk",
              "link": "/api/v1/people/8478498"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8468685,
              "fullName": "Henrik Lundqvist",
              "link": "/api/v1/people/8468685"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 13
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS429",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (24) Snap Shot, assists: David Krejci (14), Jake DeBrusk (6)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8478075,
              "fullName": "Anders Bjork",
              "link": "/api/v1/people/8478075"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8478443,
              "fullName": "Brandon Carlo",
              "link": "/api/v1/people/8478443"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 65,
          "y": -19
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS437",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (25) Slap Shot, assists: Anders Bjork (3), Brandon Carlo (8)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8479325,
              "fullName": "Charlie McAvoy",
              "link": "/api/v1/people/8479325"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8474651,
              "fullName": "Braden Holtby",
              "link": "/api/v1/people/8474651"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": -9
        },
        "result": {
          "event": "Goal",
          "eventCode": "WSH39",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (26) Snap Shot, assists: Charlie McAvoy (10), Brad Marchand (29)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": -7
        },
        "result": {
          "event": "Goal",
          "eventCode": "FLA411",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (27) Tip-In, assists: Brad Marchand (31), Torey Krug (18)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 17
          }
        ],
        "coordinates": {
          "x": 7,
          "y": -25
        },
        "result": {
          "event": "Goal",
          "eventCode": "FLA816",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (28) Snap Shot, assists: Brad Marchand (32), Patrice Bergeron (17)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8476999,
              "fullName": "Linus Ullmark",
              "link": "/api/v1/people/8476999"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -12
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS80",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (29) Wrist Shot, assists: Brad Marchand (39), Patrice Bergeron (18)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8478007,
              "fullName": "Elvis Merzlikins",
              "link": "/api/v1/people/8478007"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 37,
          "y": 28
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS387",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (30) Slap Shot, assists: Torey Krug (22), Patrice Bergeron (19)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8469608,
              "fullName": "Mike Smith",
              "link": "/api/v1/people/8469608"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 63,
          "y": 23
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS63",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (31) Slap Shot, assists: Torey Krug (23), Brad Marchand (40)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8479325,
              "fullName": "Charlie McAvoy",
              "link": "/api/v1/people/8479325"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8476891,
              "fullName": "Matt Grzelcyk",
              "link": "/api/v1/people/8476891"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 62,
          "y": -25
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH14",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (32) Slap Shot, assists: Charlie McAvoy (14), Matt Grzelcyk (10)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8475807,
              "fullName": "Joakim Nordstrom",
              "link": "/api/v1/people/8475807"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8476374,
              "fullName": "Sean Kuraly",
              "link": "/api/v1/people/8476374"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8476316,
              "fullName": "Laurent Brossoit",
              "link": "/api/v1/people/8476316"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS99",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (33) Snap Shot, assists: Joakim Nordstrom (2), Sean Kuraly (13)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 42
          },
          {
            "player": {
              "id": 8476316,
              "fullName": "Laurent Brossoit",
              "link": "/api/v1/people/8476316"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 39,
          "y": 20
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS339",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (34) Slap Shot, assists: Torey Krug (25), Brad Marchand (42)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8478498,
              "fullName": "Jake DeBrusk",
              "link": "/api/v1/people/8478498"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8476316,
              "fullName": "Laurent Brossoit",
              "link": "/api/v1/people/8476316"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": 0
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS590",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (35) Snap Shot, assists: David Krejci (21), Jake DeBrusk (11)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8470638,
              "fullName": "Patrice Bergeron",
              "link": "/api/v1/people/8470638"
            },
            "playerType": "Assist",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8479394,
              "fullName": "Carter Hart",
              "link": "/api/v1/people/8479394"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": 0
        },
        "result": {
          "event": "Goal",
          "eventCode": "PHI268",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (36) Backhand, assists: Brad Marchand (43), Patrice Bergeron (20)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 44
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8476899,
              "fullName": "Matt Murray",
              "link": "/api/v1/people/8476899"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": -11
        },
        "result": {
          "event": "Goal",
          "eventCode": "PIT93",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (37) Wrist Shot, assists: Brad Marchand (44), Torey Krug (27)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 38
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8478498,
              "fullName": "Jake DeBrusk",
              "link": "/api/v1/people/8478498"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8471227,
              "fullName": "Devan Dubnyk",
              "link": "/api/v1/people/8471227"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": 11
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN559",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (38) Wrist Shot, assists: David Krejci (24), Jake DeBrusk (15)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 47
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 75,
          "y": -8
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS75",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (39) Wrist Shot, assists: Brad Marchand (47)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8476374,
              "fullName": "Sean Kuraly",
              "link": "/api/v1/people/8476374"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": -9
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS225",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (40) Wrist Shot, assists: Sean Kuraly (15)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": 17
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS454",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (41) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 42
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 50
          },
          {
            "player": {
              "id": 8473541,
              "fullName": "Jonathan Bernier",
              "link": "/api/v1/people/8473541"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 9
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS448",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (42) Snap Shot, assists: Brad Marchand (50)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8471276,
              "fullName": "David Krejci",
              "link": "/api/v1/people/8471276"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8469608,
              "fullName": "Mike Smith",
              "link": "/api/v1/people/8469608"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": 2
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM796",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (43) Backhand, assists: David Krejci (27), Torey Krug (32)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 44
          },
          {
            "player": {
              "id": 8476891,
              "fullName": "Matt Grzelcyk",
              "link": "/api/v1/people/8476891"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8474593,
              "fullName": "Jacob Markstrom",
              "link": "/api/v1/people/8474593"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 85,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "VAN76",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (44) Wrist Shot, assists: Matt Grzelcyk (15)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 45
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 54
          },
          {
            "player": {
              "id": 8474593,
              "fullName": "Jacob Markstrom",
              "link": "/api/v1/people/8474593"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 84,
          "y": 24
        },
        "result": {
          "event": "Goal",
          "eventCode": "VAN580",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (45) Slap Shot, assists: Torey Krug (34), Brad Marchand (54)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 46
          },
          {
            "player": {
              "id": 8477941,
              "fullName": "Nick Ritchie",
              "link": "/api/v1/people/8477941"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8478468,
              "fullName": "Jeremy Lauzon",
              "link": "/api/v1/people/8478468"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8471750,
              "fullName": "Ben Bishop",
              "link": "/api/v1/people/8471750"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -2
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS482",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (46) Deflected, assists: Nick Ritchie (12), Jeremy Lauzon (1)",
          "secondaryType": "Deflected",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 47
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8478443,
              "fullName": "Brandon Carlo",
              "link": "/api/v1/people/8478443"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8473575,
              "fullName": "Semyon Varlamov",
              "link": "/api/v1/people/8473575"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 65,
          "y": -35
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYI63",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (47) Slap Shot, assists: Torey Krug (36), Brandon Carlo (15)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477956,
              "fullName": "David Pastrnak",
              "link": "/api/v1/people/8477956"
            },
            "playerType": "Scorer",
            "seasonTotal": 48
          },
          {
            "player": {
              "id": 8476792,
              "fullName": "Torey Krug",
              "link": "/api/v1/people/8476792"
            },
            "playerType": "Assist",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8473419,
              "fullName": "Brad Marchand",
              "link": "/api/v1/people/8473419"
            },
            "playerType": "Assist",
            "seasonTotal": 58
          },
          {
            "player": {
              "id": 8476883,
              "fullName": "Andrei Vasilevskiy",
              "link": "/api/v1/people/8476883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 34,
          "y": 25
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS449",
          "eventTypeId": "GOAL",
          "description": "David Pastrnak (48) Slap Shot, assists: Torey Krug (40), Brad Marchand (58)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      }
    ]
  },
  {
    "name": "Auston Matthews",
    "id": 8479318,
    "team": "TOR",
    "teamLogo": "tor-logo",
    "position": "C",
    "domId": "matthews",
    "isWinner": false,
    "missedGames": [],
    "totalGoals": 47,
    "gamesPlayed": 70,
    "gamesRemaining": 12,
    "gameArray": [
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020001,
          "link": "/api/v1/game/2019020001/feed/live",
          "content": {
            "link": "/api/v1/game/2019020001/content"
          }
        },
        "index": 69
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020015,
          "link": "/api/v1/game/2019020015/feed/live",
          "content": {
            "link": "/api/v1/game/2019020015/content"
          }
        },
        "index": 68
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020019,
          "link": "/api/v1/game/2019020019/feed/live",
          "content": {
            "link": "/api/v1/game/2019020019/content"
          }
        },
        "index": 67
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020034,
          "link": "/api/v1/game/2019020034/feed/live",
          "content": {
            "link": "/api/v1/game/2019020034/content"
          }
        },
        "index": 66
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020047,
          "link": "/api/v1/game/2019020047/feed/live",
          "content": {
            "link": "/api/v1/game/2019020047/content"
          }
        },
        "index": 65
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020066,
          "link": "/api/v1/game/2019020066/feed/live",
          "content": {
            "link": "/api/v1/game/2019020066/content"
          }
        },
        "index": 64
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020085,
          "link": "/api/v1/game/2019020085/feed/live",
          "content": {
            "link": "/api/v1/game/2019020085/content"
          }
        },
        "index": 63
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020093,
          "link": "/api/v1/game/2019020093/feed/live",
          "content": {
            "link": "/api/v1/game/2019020093/content"
          }
        },
        "index": 62
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020115,
          "link": "/api/v1/game/2019020115/feed/live",
          "content": {
            "link": "/api/v1/game/2019020115/content"
          }
        },
        "index": 61
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020129,
          "link": "/api/v1/game/2019020129/feed/live",
          "content": {
            "link": "/api/v1/game/2019020129/content"
          }
        },
        "index": 60
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020133,
          "link": "/api/v1/game/2019020133/feed/live",
          "content": {
            "link": "/api/v1/game/2019020133/content"
          }
        },
        "index": 59
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020156,
          "link": "/api/v1/game/2019020156/feed/live",
          "content": {
            "link": "/api/v1/game/2019020156/content"
          }
        },
        "index": 58
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020163,
          "link": "/api/v1/game/2019020163/feed/live",
          "content": {
            "link": "/api/v1/game/2019020163/content"
          }
        },
        "index": 57
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020180,
          "link": "/api/v1/game/2019020180/feed/live",
          "content": {
            "link": "/api/v1/game/2019020180/content"
          }
        },
        "index": 56
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020209,
          "link": "/api/v1/game/2019020209/feed/live",
          "content": {
            "link": "/api/v1/game/2019020209/content"
          }
        },
        "index": 55
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020228,
          "link": "/api/v1/game/2019020228/feed/live",
          "content": {
            "link": "/api/v1/game/2019020228/content"
          }
        },
        "index": 54
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020237,
          "link": "/api/v1/game/2019020237/feed/live",
          "content": {
            "link": "/api/v1/game/2019020237/content"
          }
        },
        "index": 53
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020253,
          "link": "/api/v1/game/2019020253/feed/live",
          "content": {
            "link": "/api/v1/game/2019020253/content"
          }
        },
        "index": 52
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020268,
          "link": "/api/v1/game/2019020268/feed/live",
          "content": {
            "link": "/api/v1/game/2019020268/content"
          }
        },
        "index": 51
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020282,
          "link": "/api/v1/game/2019020282/feed/live",
          "content": {
            "link": "/api/v1/game/2019020282/content"
          }
        },
        "index": 50
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020294,
          "link": "/api/v1/game/2019020294/feed/live",
          "content": {
            "link": "/api/v1/game/2019020294/content"
          }
        },
        "index": 49
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020308,
          "link": "/api/v1/game/2019020308/feed/live",
          "content": {
            "link": "/api/v1/game/2019020308/content"
          }
        },
        "index": 48
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020329,
          "link": "/api/v1/game/2019020329/feed/live",
          "content": {
            "link": "/api/v1/game/2019020329/content"
          }
        },
        "index": 47
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020343,
          "link": "/api/v1/game/2019020343/feed/live",
          "content": {
            "link": "/api/v1/game/2019020343/content"
          }
        },
        "index": 46
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020357,
          "link": "/api/v1/game/2019020357/feed/live",
          "content": {
            "link": "/api/v1/game/2019020357/content"
          }
        },
        "index": 45
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020379,
          "link": "/api/v1/game/2019020379/feed/live",
          "content": {
            "link": "/api/v1/game/2019020379/content"
          }
        },
        "index": 44
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020392,
          "link": "/api/v1/game/2019020392/feed/live",
          "content": {
            "link": "/api/v1/game/2019020392/content"
          }
        },
        "index": 43
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020405,
          "link": "/api/v1/game/2019020405/feed/live",
          "content": {
            "link": "/api/v1/game/2019020405/content"
          }
        },
        "index": 42
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020428,
          "link": "/api/v1/game/2019020428/feed/live",
          "content": {
            "link": "/api/v1/game/2019020428/content"
          }
        },
        "index": 41
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020434,
          "link": "/api/v1/game/2019020434/feed/live",
          "content": {
            "link": "/api/v1/game/2019020434/content"
          }
        },
        "index": 40
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020459,
          "link": "/api/v1/game/2019020459/feed/live",
          "content": {
            "link": "/api/v1/game/2019020459/content"
          }
        },
        "index": 39
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020481,
          "link": "/api/v1/game/2019020481/feed/live",
          "content": {
            "link": "/api/v1/game/2019020481/content"
          }
        },
        "index": 38
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020494,
          "link": "/api/v1/game/2019020494/feed/live",
          "content": {
            "link": "/api/v1/game/2019020494/content"
          }
        },
        "index": 37
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020511,
          "link": "/api/v1/game/2019020511/feed/live",
          "content": {
            "link": "/api/v1/game/2019020511/content"
          }
        },
        "index": 36
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020525,
          "link": "/api/v1/game/2019020525/feed/live",
          "content": {
            "link": "/api/v1/game/2019020525/content"
          }
        },
        "index": 35
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020550,
          "link": "/api/v1/game/2019020550/feed/live",
          "content": {
            "link": "/api/v1/game/2019020550/content"
          }
        },
        "index": 34
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020556,
          "link": "/api/v1/game/2019020556/feed/live",
          "content": {
            "link": "/api/v1/game/2019020556/content"
          }
        },
        "index": 33
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020569,
          "link": "/api/v1/game/2019020569/feed/live",
          "content": {
            "link": "/api/v1/game/2019020569/content"
          }
        },
        "index": 32
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020583,
          "link": "/api/v1/game/2019020583/feed/live",
          "content": {
            "link": "/api/v1/game/2019020583/content"
          }
        },
        "index": 31
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020593,
          "link": "/api/v1/game/2019020593/feed/live",
          "content": {
            "link": "/api/v1/game/2019020593/content"
          }
        },
        "index": 30
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020616,
          "link": "/api/v1/game/2019020616/feed/live",
          "content": {
            "link": "/api/v1/game/2019020616/content"
          }
        },
        "index": 29
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020633,
          "link": "/api/v1/game/2019020633/feed/live",
          "content": {
            "link": "/api/v1/game/2019020633/content"
          }
        },
        "index": 28
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020646,
          "link": "/api/v1/game/2019020646/feed/live",
          "content": {
            "link": "/api/v1/game/2019020646/content"
          }
        },
        "index": 27
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020659,
          "link": "/api/v1/game/2019020659/feed/live",
          "content": {
            "link": "/api/v1/game/2019020659/content"
          }
        },
        "index": 26
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020675,
          "link": "/api/v1/game/2019020675/feed/live",
          "content": {
            "link": "/api/v1/game/2019020675/content"
          }
        },
        "index": 25
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020707,
          "link": "/api/v1/game/2019020707/feed/live",
          "content": {
            "link": "/api/v1/game/2019020707/content"
          }
        },
        "index": 24
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020715,
          "link": "/api/v1/game/2019020715/feed/live",
          "content": {
            "link": "/api/v1/game/2019020715/content"
          }
        },
        "index": 23
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020728,
          "link": "/api/v1/game/2019020728/feed/live",
          "content": {
            "link": "/api/v1/game/2019020728/content"
          }
        },
        "index": 22
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020747,
          "link": "/api/v1/game/2019020747/feed/live",
          "content": {
            "link": "/api/v1/game/2019020747/content"
          }
        },
        "index": 21
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020770,
          "link": "/api/v1/game/2019020770/feed/live",
          "content": {
            "link": "/api/v1/game/2019020770/content"
          }
        },
        "index": 20
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020777,
          "link": "/api/v1/game/2019020777/feed/live",
          "content": {
            "link": "/api/v1/game/2019020777/content"
          }
        },
        "index": 19
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020795,
          "link": "/api/v1/game/2019020795/feed/live",
          "content": {
            "link": "/api/v1/game/2019020795/content"
          }
        },
        "index": 18
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020809,
          "link": "/api/v1/game/2019020809/feed/live",
          "content": {
            "link": "/api/v1/game/2019020809/content"
          }
        },
        "index": 17
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020825,
          "link": "/api/v1/game/2019020825/feed/live",
          "content": {
            "link": "/api/v1/game/2019020825/content"
          }
        },
        "index": 16
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020839,
          "link": "/api/v1/game/2019020839/feed/live",
          "content": {
            "link": "/api/v1/game/2019020839/content"
          }
        },
        "index": 15
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020845,
          "link": "/api/v1/game/2019020845/feed/live",
          "content": {
            "link": "/api/v1/game/2019020845/content"
          }
        },
        "index": 14
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020867,
          "link": "/api/v1/game/2019020867/feed/live",
          "content": {
            "link": "/api/v1/game/2019020867/content"
          }
        },
        "index": 13
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020881,
          "link": "/api/v1/game/2019020881/feed/live",
          "content": {
            "link": "/api/v1/game/2019020881/content"
          }
        },
        "index": 12
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020901,
          "link": "/api/v1/game/2019020901/feed/live",
          "content": {
            "link": "/api/v1/game/2019020901/content"
          }
        },
        "index": 11
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020913,
          "link": "/api/v1/game/2019020913/feed/live",
          "content": {
            "link": "/api/v1/game/2019020913/content"
          }
        },
        "index": 10
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020921,
          "link": "/api/v1/game/2019020921/feed/live",
          "content": {
            "link": "/api/v1/game/2019020921/content"
          }
        },
        "index": 9
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020933,
          "link": "/api/v1/game/2019020933/feed/live",
          "content": {
            "link": "/api/v1/game/2019020933/content"
          }
        },
        "index": 8
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020951,
          "link": "/api/v1/game/2019020951/feed/live",
          "content": {
            "link": "/api/v1/game/2019020951/content"
          }
        },
        "index": 7
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020970,
          "link": "/api/v1/game/2019020970/feed/live",
          "content": {
            "link": "/api/v1/game/2019020970/content"
          }
        },
        "index": 6
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020987,
          "link": "/api/v1/game/2019020987/feed/live",
          "content": {
            "link": "/api/v1/game/2019020987/content"
          }
        },
        "index": 5
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021003,
          "link": "/api/v1/game/2019021003/feed/live",
          "content": {
            "link": "/api/v1/game/2019021003/content"
          }
        },
        "index": 4
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021028,
          "link": "/api/v1/game/2019021028/feed/live",
          "content": {
            "link": "/api/v1/game/2019021028/content"
          }
        },
        "index": 3
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021041,
          "link": "/api/v1/game/2019021041/feed/live",
          "content": {
            "link": "/api/v1/game/2019021041/content"
          }
        },
        "index": 2
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021048,
          "link": "/api/v1/game/2019021048/feed/live",
          "content": {
            "link": "/api/v1/game/2019021048/content"
          }
        },
        "index": 1
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021070,
          "link": "/api/v1/game/2019021070/feed/live",
          "content": {
            "link": "/api/v1/game/2019021070/content"
          }
        },
        "index": 0
      }
    ],
    "sumArray": [
      0,
      2,
      3,
      5,
      5,
      6,
      6,
      7,
      7,
      7,
      8,
      8,
      9,
      9,
      11,
      11,
      12,
      13,
      13,
      13,
      13,
      14,
      14,
      14,
      15,
      16,
      16,
      16,
      16,
      16,
      16,
      18,
      19,
      19,
      19,
      21,
      21,
      23,
      24,
      24,
      26,
      27,
      27,
      28,
      29,
      31,
      31,
      34,
      34,
      34,
      35,
      36,
      36,
      37,
      39,
      40,
      40,
      40,
      41,
      42,
      42,
      43,
      43,
      43,
      43,
      44,
      45,
      46,
      46,
      46,
      47
    ],
    "scoringPlays": [
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 83,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR480",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (1) Wrist Shot, assists: William Nylander (1), Morgan Rielly (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 65,
          "y": -20
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR610",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (2) Slap Shot, assists: Mitchell Marner (1), John Tavares (1)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8476914,
              "fullName": "Joonas Korpisalo",
              "link": "/api/v1/people/8476914"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 66,
          "y": -25
        },
        "result": {
          "event": "Goal",
          "eventCode": "CBJ709",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (3) Wrist Shot, assists: Morgan Rielly (4), Mitchell Marner (2)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476879,
              "fullName": "Cody Ceci",
              "link": "/api/v1/people/8476879"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 48,
          "y": 13
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR36",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (4) Wrist Shot, assists: Morgan Rielly (5), Cody Ceci (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 15
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR1151",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (5) Wrist Shot, assists: Tyson Barrie (4), Mitchell Marner (3)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8474162,
              "fullName": "Jake Muzzin",
              "link": "/api/v1/people/8474162"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8476883,
              "fullName": "Andrei Vasilevskiy",
              "link": "/api/v1/people/8476883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 84,
          "y": -8
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR223",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (6) Wrist Shot, assists: Jake Muzzin (2), William Nylander (3)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8471227,
              "fullName": "Devan Dubnyk",
              "link": "/api/v1/people/8471227"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR568",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (7) Wrist Shot, assists: Mitchell Marner (6), Morgan Rielly (9)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8477341,
              "fullName": "Andreas Johnsson",
              "link": "/api/v1/people/8477341"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8474162,
              "fullName": "Jake Muzzin",
              "link": "/api/v1/people/8474162"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8476914,
              "fullName": "Joonas Korpisalo",
              "link": "/api/v1/people/8476914"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 6
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR218",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (8) Deflected, assists: Andreas Johnsson (4), Jake Muzzin (6)",
          "secondaryType": "Deflected",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8474889,
              "fullName": "Martin Jones",
              "link": "/api/v1/people/8474889"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 57,
          "y": 18
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR712",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (9) Wrist Shot, assists: Mitchell Marner (11)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8474651,
              "fullName": "Braden Holtby",
              "link": "/api/v1/people/8474651"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 67,
          "y": 3
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR436",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (10) Tip-In, assists: Tyson Barrie (5), Morgan Rielly (11)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8474651,
              "fullName": "Braden Holtby",
              "link": "/api/v1/people/8474651"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 65,
          "y": 8
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR603",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (11) Wrist Shot, assists: William Nylander (6), Mitchell Marner (13)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8477341,
              "fullName": "Andreas Johnsson",
              "link": "/api/v1/people/8477341"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8471734,
              "fullName": "Jonathan Quick",
              "link": "/api/v1/people/8471734"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR667",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (12) Wrist Shot, assists: William Nylander (7), Andreas Johnsson (6)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8476876,
              "fullName": "Malcolm Subban",
              "link": "/api/v1/people/8476876"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 58,
          "y": -24
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR679",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (13) Wrist Shot, assists: John Tavares (5), Morgan Rielly (13)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8474162,
              "fullName": "Jake Muzzin",
              "link": "/api/v1/people/8474162"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8471695,
              "fullName": "Tuukka Rask",
              "link": "/api/v1/people/8471695"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 70,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR478",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (14) Tip-In, assists: Jake Muzzin (8), William Nylander (9)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8481624,
              "fullName": "Ilya Mikheyev",
              "link": "/api/v1/people/8481624"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8475311,
              "fullName": "Darcy Kuemper",
              "link": "/api/v1/people/8475311"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": 7
        },
        "result": {
          "event": "Goal",
          "eventCode": "ARI477",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (15) Wrist Shot, assists: William Nylander (10), Ilya Mikheyev (10)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8478408,
              "fullName": "Travis Dermott",
              "link": "/api/v1/people/8478408"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8475831,
              "fullName": "Philipp Grubauer",
              "link": "/api/v1/people/8475831"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 57,
          "y": -5
        },
        "result": {
          "event": "Goal",
          "eventCode": "COL39",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (16) Wrist Shot, assists: Tyson Barrie (8), Travis Dermott (2)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8476857,
              "fullName": "Pontus Aberg",
              "link": "/api/v1/people/8476857"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8476412,
              "fullName": "Jordan Binnington",
              "link": "/api/v1/people/8476412"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 47,
          "y": 20
        },
        "result": {
          "event": "Goal",
          "eventCode": "STL78",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (17) Wrist Shot, assists: William Nylander (12), Pontus Aberg (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8474596,
              "fullName": "Jake Allen",
              "link": "/api/v1/people/8474596"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 87,
          "y": -7
        },
        "result": {
          "event": "Goal",
          "eventCode": "STL237",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (18) Tip-In, assists: Mitchell Marner (16)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8475718,
              "fullName": "Justin Holl",
              "link": "/api/v1/people/8475718"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8474593,
              "fullName": "Jacob Markstrom",
              "link": "/api/v1/people/8474593"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "VAN351",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (19) Wrist Shot, assists: John Tavares (11), Justin Holl (8)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8476999,
              "fullName": "Linus Ullmark",
              "link": "/api/v1/people/8476999"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 62,
          "y": 9
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR324",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (20) Wrist Shot, assists: Morgan Rielly (20), Mitchell Marner (20)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8476999,
              "fullName": "Linus Ullmark",
              "link": "/api/v1/people/8476999"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": -11
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR508",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (21) Backhand, assists: William Nylander (13), Tyson Barrie (11)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475717,
              "fullName": "Calvin Pickard",
              "link": "/api/v1/people/8475717"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": 6
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR487",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (22) Wrist Shot, assists: John Tavares (13), Mitchell Marner (21)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8475786,
              "fullName": "Zach Hyman",
              "link": "/api/v1/people/8475786"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8475717,
              "fullName": "Calvin Pickard",
              "link": "/api/v1/people/8475717"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 75,
          "y": 2
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR657",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (23) Snap Shot, assists: Mitchell Marner (22), Zach Hyman (3)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8475786,
              "fullName": "Zach Hyman",
              "link": "/api/v1/people/8475786"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8475852,
              "fullName": "Petr Mrazek",
              "link": "/api/v1/people/8475852"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 61,
          "y": 14
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR706",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (24) Snap Shot, assists: Zach Hyman (4), Mitchell Marner (24)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8480382,
              "fullName": "Alexandar Georgiev",
              "link": "/api/v1/people/8480382"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 58,
          "y": 3
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR464",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (25) Snap Shot, assists: Mitchell Marner (26)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8480382,
              "fullName": "Alexandar Georgiev",
              "link": "/api/v1/people/8480382"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": -24
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR735",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (26) Wrist Shot, assists: William Nylander (17), Tyson Barrie (16)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8471227,
              "fullName": "Devan Dubnyk",
              "link": "/api/v1/people/8471227"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": -18
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN275",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (27) Wrist Shot, assists: William Nylander (18), Mitchell Marner (27)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8475718,
              "fullName": "Justin Holl",
              "link": "/api/v1/people/8475718"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8473575,
              "fullName": "Semyon Varlamov",
              "link": "/api/v1/people/8473575"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR346",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (28) Tip-In, assists: Mitchell Marner (28), Justin Holl (11)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8469608,
              "fullName": "Mike Smith",
              "link": "/api/v1/people/8469608"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -15
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR754",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (29) Wrist Shot, assists: Tyson Barrie (19), Mitchell Marner (30)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -8
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR17",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (30) Wrist Shot, assists: Mitchell Marner (31)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8476853,
              "fullName": "Morgan Rielly",
              "link": "/api/v1/people/8476853"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 61,
          "y": -28
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR549",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (31) Slap Shot, assists: Morgan Rielly (23)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 0
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": -5
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR737",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews - Backhand",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8475839,
              "fullName": "Louis Domingue",
              "link": "/api/v1/people/8475839"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": -8
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR412",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (32) Tip-In, assists: Mitchell Marner (32), John Tavares (22)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8475786,
              "fullName": "Zach Hyman",
              "link": "/api/v1/people/8475786"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8471239,
              "fullName": "Cory Schneider",
              "link": "/api/v1/people/8471239"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -13
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR614",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (33) Slap Shot, assists: Mitchell Marner (33), Zach Hyman (9)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 34
          }
        ],
        "coordinates": {
          "x": 30,
          "y": -30
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR807",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (34) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 35
          }
        ],
        "coordinates": {
          "x": 32,
          "y": 23
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH631",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (35) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8471750,
              "fullName": "Ben Bishop",
              "link": "/api/v1/people/8471750"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 75,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "DAL87",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (36) Wrist Shot, assists: Mitchell Marner (36), Tyson Barrie (25)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8475786,
              "fullName": "Zach Hyman",
              "link": "/api/v1/people/8475786"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": 2
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR478",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (37) Wrist Shot, assists: Zach Hyman (12), Mitchell Marner (39)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 38
          },
          {
            "player": {
              "id": 8477341,
              "fullName": "Andreas Johnsson",
              "link": "/api/v1/people/8477341"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8477953,
              "fullName": "Kasperi Kapanen",
              "link": "/api/v1/people/8477953"
            },
            "playerType": "Assist",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8478048,
              "fullName": "Igor Shesterkin",
              "link": "/api/v1/people/8478048"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -2
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR557",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (38) Backhand, assists: Andreas Johnsson (13), Kasperi Kapanen (20)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8478048,
              "fullName": "Igor Shesterkin",
              "link": "/api/v1/people/8478048"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": -14
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR681",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (39) Slap Shot, assists: John Tavares (24), Mitchell Marner (40)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 42
          },
          {
            "player": {
              "id": 8468011,
              "fullName": "Ryan Miller",
              "link": "/api/v1/people/8468011"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -21
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR511",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (40) Slap Shot, assists: John Tavares (25), Mitchell Marner (42)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8475197,
              "fullName": "Tyson Barrie",
              "link": "/api/v1/people/8475197"
            },
            "playerType": "Assist",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8471750,
              "fullName": "Ben Bishop",
              "link": "/api/v1/people/8471750"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": -12
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR243",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (41) Wrist Shot, assists: William Nylander (25), Tyson Barrie (28)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 42
          },
          {
            "player": {
              "id": 8475786,
              "fullName": "Zach Hyman",
              "link": "/api/v1/people/8475786"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8478408,
              "fullName": "Travis Dermott",
              "link": "/api/v1/people/8478408"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8477405,
              "fullName": "Marcus Hogberg",
              "link": "/api/v1/people/8477405"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": 8
        },
        "result": {
          "event": "Goal",
          "eventCode": "OTT17",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (42) Wrist Shot, assists: Zach Hyman (15), Travis Dermott (5)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8477939,
              "fullName": "William Nylander",
              "link": "/api/v1/people/8477939"
            },
            "playerType": "Assist",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8474162,
              "fullName": "Jake Muzzin",
              "link": "/api/v1/people/8474162"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8477465,
              "fullName": "Tristan Jarry",
              "link": "/api/v1/people/8477465"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 64,
          "y": -24
        },
        "result": {
          "event": "Goal",
          "eventCode": "PIT453",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (43) Slap Shot, assists: William Nylander (26), Jake Muzzin (14)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 44
          },
          {
            "player": {
              "id": 8475786,
              "fullName": "Zach Hyman",
              "link": "/api/v1/people/8475786"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 48
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": -10
        },
        "result": {
          "event": "Goal",
          "eventCode": "FLA353",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (44) Slap Shot, assists: Zach Hyman (16), Mitchell Marner (48)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 45
          },
          {
            "player": {
              "id": 8478408,
              "fullName": "Travis Dermott",
              "link": "/api/v1/people/8478408"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8477967,
              "fullName": "Thatcher Demko",
              "link": "/api/v1/people/8477967"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": 24
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR22",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (45) Wrist Shot, assists: Travis Dermott (6)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 46
          },
          {
            "player": {
              "id": 8475716,
              "fullName": "Martin Marincin",
              "link": "/api/v1/people/8475716"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8474889,
              "fullName": "Martin Jones",
              "link": "/api/v1/people/8474889"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -5
        },
        "result": {
          "event": "Goal",
          "eventCode": "SJS232",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (46) Backhand, assists: Martin Marincin (3)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8479318,
              "fullName": "Auston Matthews",
              "link": "/api/v1/people/8479318"
            },
            "playerType": "Scorer",
            "seasonTotal": 47
          },
          {
            "player": {
              "id": 8478483,
              "fullName": "Mitchell Marner",
              "link": "/api/v1/people/8478483"
            },
            "playerType": "Assist",
            "seasonTotal": 51
          },
          {
            "player": {
              "id": 8475166,
              "fullName": "John Tavares",
              "link": "/api/v1/people/8475166"
            },
            "playerType": "Assist",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8476883,
              "fullName": "Andrei Vasilevskiy",
              "link": "/api/v1/people/8476883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": -18
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR498",
          "eventTypeId": "GOAL",
          "description": "Auston Matthews (47) Wrist Shot, assists: Mitchell Marner (51), John Tavares (34)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      }
    ]
  },
  {
    "name": "Leon Draisaitl",
    "id": 8477934,
    "team": "EDM",
    "domId": "draisaitl",
    "teamLogo": "edm-logo",
    "position": "C",
    "isWinner": false,
    "missedGames": [],
    "totalGoals": 43,
    "gamesPlayed": 71,
    "gamesRemaining": 11,
    "gameArray": [
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020003,
          "link": "/api/v1/game/2019020003/feed/live",
          "content": {
            "link": "/api/v1/game/2019020003/content"
          }
        },
        "index": 70
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020029,
          "link": "/api/v1/game/2019020029/feed/live",
          "content": {
            "link": "/api/v1/game/2019020029/content"
          }
        },
        "index": 69
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020037,
          "link": "/api/v1/game/2019020037/feed/live",
          "content": {
            "link": "/api/v1/game/2019020037/content"
          }
        },
        "index": 68
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020049,
          "link": "/api/v1/game/2019020049/feed/live",
          "content": {
            "link": "/api/v1/game/2019020049/content"
          }
        },
        "index": 67
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020061,
          "link": "/api/v1/game/2019020061/feed/live",
          "content": {
            "link": "/api/v1/game/2019020061/content"
          }
        },
        "index": 66
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020084,
          "link": "/api/v1/game/2019020084/feed/live",
          "content": {
            "link": "/api/v1/game/2019020084/content"
          }
        },
        "index": 65
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020095,
          "link": "/api/v1/game/2019020095/feed/live",
          "content": {
            "link": "/api/v1/game/2019020095/content"
          }
        },
        "index": 64
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020111,
          "link": "/api/v1/game/2019020111/feed/live",
          "content": {
            "link": "/api/v1/game/2019020111/content"
          }
        },
        "index": 63
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020127,
          "link": "/api/v1/game/2019020127/feed/live",
          "content": {
            "link": "/api/v1/game/2019020127/content"
          }
        },
        "index": 62
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020139,
          "link": "/api/v1/game/2019020139/feed/live",
          "content": {
            "link": "/api/v1/game/2019020139/content"
          }
        },
        "index": 61
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020154,
          "link": "/api/v1/game/2019020154/feed/live",
          "content": {
            "link": "/api/v1/game/2019020154/content"
          }
        },
        "index": 60
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020170,
          "link": "/api/v1/game/2019020170/feed/live",
          "content": {
            "link": "/api/v1/game/2019020170/content"
          }
        },
        "index": 59
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020184,
          "link": "/api/v1/game/2019020184/feed/live",
          "content": {
            "link": "/api/v1/game/2019020184/content"
          }
        },
        "index": 58
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020189,
          "link": "/api/v1/game/2019020189/feed/live",
          "content": {
            "link": "/api/v1/game/2019020189/content"
          }
        },
        "index": 57
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020204,
          "link": "/api/v1/game/2019020204/feed/live",
          "content": {
            "link": "/api/v1/game/2019020204/content"
          }
        },
        "index": 56
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020223,
          "link": "/api/v1/game/2019020223/feed/live",
          "content": {
            "link": "/api/v1/game/2019020223/content"
          }
        },
        "index": 55
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020236,
          "link": "/api/v1/game/2019020236/feed/live",
          "content": {
            "link": "/api/v1/game/2019020236/content"
          }
        },
        "index": 54
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020251,
          "link": "/api/v1/game/2019020251/feed/live",
          "content": {
            "link": "/api/v1/game/2019020251/content"
          }
        },
        "index": 53
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020269,
          "link": "/api/v1/game/2019020269/feed/live",
          "content": {
            "link": "/api/v1/game/2019020269/content"
          }
        },
        "index": 52
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020280,
          "link": "/api/v1/game/2019020280/feed/live",
          "content": {
            "link": "/api/v1/game/2019020280/content"
          }
        },
        "index": 51
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020290,
          "link": "/api/v1/game/2019020290/feed/live",
          "content": {
            "link": "/api/v1/game/2019020290/content"
          }
        },
        "index": 50
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020301,
          "link": "/api/v1/game/2019020301/feed/live",
          "content": {
            "link": "/api/v1/game/2019020301/content"
          }
        },
        "index": 49
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020330,
          "link": "/api/v1/game/2019020330/feed/live",
          "content": {
            "link": "/api/v1/game/2019020330/content"
          }
        },
        "index": 48
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020345,
          "link": "/api/v1/game/2019020345/feed/live",
          "content": {
            "link": "/api/v1/game/2019020345/content"
          }
        },
        "index": 47
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020360,
          "link": "/api/v1/game/2019020360/feed/live",
          "content": {
            "link": "/api/v1/game/2019020360/content"
          }
        },
        "index": 46
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020364,
          "link": "/api/v1/game/2019020364/feed/live",
          "content": {
            "link": "/api/v1/game/2019020364/content"
          }
        },
        "index": 45
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020387,
          "link": "/api/v1/game/2019020387/feed/live",
          "content": {
            "link": "/api/v1/game/2019020387/content"
          }
        },
        "index": 44
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020414,
          "link": "/api/v1/game/2019020414/feed/live",
          "content": {
            "link": "/api/v1/game/2019020414/content"
          }
        },
        "index": 43
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020418,
          "link": "/api/v1/game/2019020418/feed/live",
          "content": {
            "link": "/api/v1/game/2019020418/content"
          }
        },
        "index": 42
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020436,
          "link": "/api/v1/game/2019020436/feed/live",
          "content": {
            "link": "/api/v1/game/2019020436/content"
          }
        },
        "index": 41
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020450,
          "link": "/api/v1/game/2019020450/feed/live",
          "content": {
            "link": "/api/v1/game/2019020450/content"
          }
        },
        "index": 40
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020467,
          "link": "/api/v1/game/2019020467/feed/live",
          "content": {
            "link": "/api/v1/game/2019020467/content"
          }
        },
        "index": 39
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020479,
          "link": "/api/v1/game/2019020479/feed/live",
          "content": {
            "link": "/api/v1/game/2019020479/content"
          }
        },
        "index": 38
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020493,
          "link": "/api/v1/game/2019020493/feed/live",
          "content": {
            "link": "/api/v1/game/2019020493/content"
          }
        },
        "index": 37
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020511,
          "link": "/api/v1/game/2019020511/feed/live",
          "content": {
            "link": "/api/v1/game/2019020511/content"
          }
        },
        "index": 36
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020523,
          "link": "/api/v1/game/2019020523/feed/live",
          "content": {
            "link": "/api/v1/game/2019020523/content"
          }
        },
        "index": 35
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020537,
          "link": "/api/v1/game/2019020537/feed/live",
          "content": {
            "link": "/api/v1/game/2019020537/content"
          }
        },
        "index": 34
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020551,
          "link": "/api/v1/game/2019020551/feed/live",
          "content": {
            "link": "/api/v1/game/2019020551/content"
          }
        },
        "index": 33
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020561,
          "link": "/api/v1/game/2019020561/feed/live",
          "content": {
            "link": "/api/v1/game/2019020561/content"
          }
        },
        "index": 32
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020579,
          "link": "/api/v1/game/2019020579/feed/live",
          "content": {
            "link": "/api/v1/game/2019020579/content"
          }
        },
        "index": 31
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020590,
          "link": "/api/v1/game/2019020590/feed/live",
          "content": {
            "link": "/api/v1/game/2019020590/content"
          }
        },
        "index": 30
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020623,
          "link": "/api/v1/game/2019020623/feed/live",
          "content": {
            "link": "/api/v1/game/2019020623/content"
          }
        },
        "index": 29
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020628,
          "link": "/api/v1/game/2019020628/feed/live",
          "content": {
            "link": "/api/v1/game/2019020628/content"
          }
        },
        "index": 28
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020641,
          "link": "/api/v1/game/2019020641/feed/live",
          "content": {
            "link": "/api/v1/game/2019020641/content"
          }
        },
        "index": 27
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020659,
          "link": "/api/v1/game/2019020659/feed/live",
          "content": {
            "link": "/api/v1/game/2019020659/content"
          }
        },
        "index": 26
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020679,
          "link": "/api/v1/game/2019020679/feed/live",
          "content": {
            "link": "/api/v1/game/2019020679/content"
          }
        },
        "index": 25
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020700,
          "link": "/api/v1/game/2019020700/feed/live",
          "content": {
            "link": "/api/v1/game/2019020700/content"
          }
        },
        "index": 24
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020723,
          "link": "/api/v1/game/2019020723/feed/live",
          "content": {
            "link": "/api/v1/game/2019020723/content"
          }
        },
        "index": 23
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020745,
          "link": "/api/v1/game/2019020745/feed/live",
          "content": {
            "link": "/api/v1/game/2019020745/content"
          }
        },
        "index": 22
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020778,
          "link": "/api/v1/game/2019020778/feed/live",
          "content": {
            "link": "/api/v1/game/2019020778/content"
          }
        },
        "index": 21
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020790,
          "link": "/api/v1/game/2019020790/feed/live",
          "content": {
            "link": "/api/v1/game/2019020790/content"
          }
        },
        "index": 20
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020803,
          "link": "/api/v1/game/2019020803/feed/live",
          "content": {
            "link": "/api/v1/game/2019020803/content"
          }
        },
        "index": 19
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020824,
          "link": "/api/v1/game/2019020824/feed/live",
          "content": {
            "link": "/api/v1/game/2019020824/content"
          }
        },
        "index": 18
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020837,
          "link": "/api/v1/game/2019020837/feed/live",
          "content": {
            "link": "/api/v1/game/2019020837/content"
          }
        },
        "index": 17
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020851,
          "link": "/api/v1/game/2019020851/feed/live",
          "content": {
            "link": "/api/v1/game/2019020851/content"
          }
        },
        "index": 16
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020875,
          "link": "/api/v1/game/2019020875/feed/live",
          "content": {
            "link": "/api/v1/game/2019020875/content"
          }
        },
        "index": 15
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020882,
          "link": "/api/v1/game/2019020882/feed/live",
          "content": {
            "link": "/api/v1/game/2019020882/content"
          }
        },
        "index": 14
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020898,
          "link": "/api/v1/game/2019020898/feed/live",
          "content": {
            "link": "/api/v1/game/2019020898/content"
          }
        },
        "index": 13
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020908,
          "link": "/api/v1/game/2019020908/feed/live",
          "content": {
            "link": "/api/v1/game/2019020908/content"
          }
        },
        "index": 12
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020929,
          "link": "/api/v1/game/2019020929/feed/live",
          "content": {
            "link": "/api/v1/game/2019020929/content"
          }
        },
        "index": 11
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020946,
          "link": "/api/v1/game/2019020946/feed/live",
          "content": {
            "link": "/api/v1/game/2019020946/content"
          }
        },
        "index": 10
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020966,
          "link": "/api/v1/game/2019020966/feed/live",
          "content": {
            "link": "/api/v1/game/2019020966/content"
          }
        },
        "index": 9
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020980,
          "link": "/api/v1/game/2019020980/feed/live",
          "content": {
            "link": "/api/v1/game/2019020980/content"
          }
        },
        "index": 8
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020982,
          "link": "/api/v1/game/2019020982/feed/live",
          "content": {
            "link": "/api/v1/game/2019020982/content"
          }
        },
        "index": 7
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019021009,
          "link": "/api/v1/game/2019021009/feed/live",
          "content": {
            "link": "/api/v1/game/2019021009/content"
          }
        },
        "index": 6
      },
      {
        "goals": 4,
        "game": {
          "gamePk": 2019021018,
          "link": "/api/v1/game/2019021018/feed/live",
          "content": {
            "link": "/api/v1/game/2019021018/content"
          }
        },
        "index": 5
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021026,
          "link": "/api/v1/game/2019021026/feed/live",
          "content": {
            "link": "/api/v1/game/2019021026/content"
          }
        },
        "index": 4
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021040,
          "link": "/api/v1/game/2019021040/feed/live",
          "content": {
            "link": "/api/v1/game/2019021040/content"
          }
        },
        "index": 3
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021058,
          "link": "/api/v1/game/2019021058/feed/live",
          "content": {
            "link": "/api/v1/game/2019021058/content"
          }
        },
        "index": 2
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021068,
          "link": "/api/v1/game/2019021068/feed/live",
          "content": {
            "link": "/api/v1/game/2019021068/content"
          }
        },
        "index": 1
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019021080,
          "link": "/api/v1/game/2019021080/feed/live",
          "content": {
            "link": "/api/v1/game/2019021080/content"
          }
        },
        "index": 0
      }
    ],
    "sumArray": [
      0,
      1,
      1,
      1,
      2,
      4,
      4,
      6,
      6,
      6,
      6,
      8,
      9,
      10,
      12,
      13,
      13,
      13,
      14,
      14,
      15,
      15,
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      18,
      18,
      19,
      19,
      19,
      20,
      20,
      21,
      21,
      21,
      22,
      22,
      22,
      23,
      23,
      24,
      25,
      25,
      25,
      27,
      27,
      27,
      29,
      29,
      29,
      29,
      31,
      32,
      32,
      33,
      34,
      34,
      35,
      36,
      37,
      37,
      39,
      43,
      43,
      43,
      43,
      43,
      43
    ],
    "scoringPlays": [
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8475178,
              "fullName": "Zack Kassian",
              "link": "/api/v1/people/8475178"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8476988,
              "fullName": "Matt Benning",
              "link": "/api/v1/people/8476988"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8474593,
              "fullName": "Jacob Markstrom",
              "link": "/api/v1/people/8474593"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM69",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (1) Snap Shot, assists: Zack Kassian (1), Matt Benning (1)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8477498,
              "fullName": "Darnell Nurse",
              "link": "/api/v1/people/8477498"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8475178,
              "fullName": "Zack Kassian",
              "link": "/api/v1/people/8475178"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": 12
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD27",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (2) Snap Shot, assists: Darnell Nurse (3), Zack Kassian (2)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 0
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 61,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD723",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl - Snap Shot",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8478451,
              "fullName": "Ethan Bear",
              "link": "/api/v1/people/8478451"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8468685,
              "fullName": "Henrik Lundqvist",
              "link": "/api/v1/people/8468685"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 85,
          "y": -9
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR599",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (3) Wrist Shot, assists: Connor McDavid (8), Ethan Bear (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8477498,
              "fullName": "Darnell Nurse",
              "link": "/api/v1/people/8477498"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          }
        ],
        "coordinates": {
          "x": 84,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR762",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (4) Wrist Shot, assists: Darnell Nurse (4)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8476472,
              "fullName": "Oscar Klefbom",
              "link": "/api/v1/people/8476472"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8479394,
              "fullName": "Carter Hart",
              "link": "/api/v1/people/8479394"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": -15
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM54",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (5) Slap Shot, assists: Connor McDavid (9), Oscar Klefbom (7)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8470880,
              "fullName": "Brian Elliott",
              "link": "/api/v1/people/8470880"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 7
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM463",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (6) Backhand, assists: Connor McDavid (11)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475178,
              "fullName": "Zack Kassian",
              "link": "/api/v1/people/8475178"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8474651,
              "fullName": "Braden Holtby",
              "link": "/api/v1/people/8474651"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": 8
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM543",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (7) Snap Shot, assists: Connor McDavid (13), Zack Kassian (5)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8474651,
              "fullName": "Braden Holtby",
              "link": "/api/v1/people/8474651"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": -13
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM691",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (8) Snap Shot, assists: Connor McDavid (14)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8478451,
              "fullName": "Ethan Bear",
              "link": "/api/v1/people/8478451"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8475683,
              "fullName": "Sergei Bobrovsky",
              "link": "/api/v1/people/8475683"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": 14
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM655",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (9) Snap Shot, assists: Ethan Bear (2), Connor McDavid (15)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8470657,
              "fullName": "Jimmy Howard",
              "link": "/api/v1/people/8470657"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -19
        },
        "result": {
          "event": "Goal",
          "eventCode": "DET437",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (10) Wrist Shot, assists: Connor McDavid (16)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8476914,
              "fullName": "Joonas Korpisalo",
              "link": "/api/v1/people/8476914"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": -10
        },
        "result": {
          "event": "Goal",
          "eventCode": "CBJ203",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (11) Wrist Shot, assists: Ryan Nugent-Hopkins (7), Alex Chiasson (3)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8475178,
              "fullName": "Zack Kassian",
              "link": "/api/v1/people/8475178"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8476914,
              "fullName": "Joonas Korpisalo",
              "link": "/api/v1/people/8476914"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "CBJ420",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (12) Snap Shot, assists: Zack Kassian (6)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8476899,
              "fullName": "Matt Murray",
              "link": "/api/v1/people/8476899"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "PIT692",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (13) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8480940,
              "fullName": "Joel Persson",
              "link": "/api/v1/people/8480940"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8471239,
              "fullName": "Cory Schneider",
              "link": "/api/v1/people/8471239"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 71,
          "y": -2
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM217",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (14) Backhand, assists: Ryan Nugent-Hopkins (9), Joel Persson (1)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8474040,
              "fullName": "Sam Gagner",
              "link": "/api/v1/people/8474040"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8478451,
              "fullName": "Ethan Bear",
              "link": "/api/v1/people/8478451"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8474889,
              "fullName": "Martin Jones",
              "link": "/api/v1/people/8474889"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 36,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "SJS245",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (15) Wrist Shot, assists: Sam Gagner (2), Ethan Bear (4)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8477498,
              "fullName": "Darnell Nurse",
              "link": "/api/v1/people/8477498"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8471750,
              "fullName": "Ben Bishop",
              "link": "/api/v1/people/8471750"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -3
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM302",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (16) Wrist Shot, assists: Connor McDavid (24), Darnell Nurse (7)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8474593,
              "fullName": "Jacob Markstrom",
              "link": "/api/v1/people/8474593"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": -23
        },
        "result": {
          "event": "Goal",
          "eventCode": "VAN371",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (17) Deflected, assists: Connor McDavid (32), Alex Chiasson (5)",
          "secondaryType": "Deflected",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8471707,
              "fullName": "James Neal",
              "link": "/api/v1/people/8471707"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8474593,
              "fullName": "Jacob Markstrom",
              "link": "/api/v1/people/8474593"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": -19
        },
        "result": {
          "event": "Goal",
          "eventCode": "VAN391",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (18) Slap Shot, assists: James Neal (4), Alex Chiasson (6)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8471707,
              "fullName": "James Neal",
              "link": "/api/v1/people/8471707"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8471734,
              "fullName": "Jonathan Quick",
              "link": "/api/v1/people/8471734"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": -17
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM84",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (19) Wrist Shot, assists: Alex Chiasson (7), James Neal (6)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8476472,
              "fullName": "Oscar Klefbom",
              "link": "/api/v1/people/8476472"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8478039,
              "fullName": "Kaapo Kahkonen",
              "link": "/api/v1/people/8478039"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 83,
          "y": -7
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN266",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (20) Wrist Shot, assists: Connor McDavid (37), Oscar Klefbom (18)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8471750,
              "fullName": "Ben Bishop",
              "link": "/api/v1/people/8471750"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": -13
        },
        "result": {
          "event": "Goal",
          "eventCode": "DAL137",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (21) Wrist Shot, assists: Alex Chiasson (8), Connor McDavid (39)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8475178,
              "fullName": "Zack Kassian",
              "link": "/api/v1/people/8475178"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 7
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM57",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (22) Snap Shot, assists: Connor McDavid (40), Zack Kassian (13)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8471707,
              "fullName": "James Neal",
              "link": "/api/v1/people/8471707"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8480382,
              "fullName": "Alexandar Georgiev",
              "link": "/api/v1/people/8480382"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 75,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM559",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (23) Wrist Shot, assists: Ryan Nugent-Hopkins (15), James Neal (8)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 24
          }
        ],
        "coordinates": {
          "x": 49,
          "y": -15
        },
        "result": {
          "event": "Goal",
          "eventCode": "BOS679",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (24) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 45
          },
          {
            "player": {
              "id": 8474636,
              "fullName": "Michael Hutchinson",
              "link": "/api/v1/people/8474636"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": -9
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR630",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (25) Wrist Shot, assists: Alex Chiasson (9), Connor McDavid (45)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8475163,
              "fullName": "Alex Chiasson",
              "link": "/api/v1/people/8475163"
            },
            "playerType": "Assist",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 47
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM264",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (26) Snap Shot, assists: Alex Chiasson (10), Connor McDavid (47)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 49
          }
        ],
        "coordinates": {
          "x": 84,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM635",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (27) Backhand, assists: Connor McDavid (49)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8474596,
              "fullName": "Jake Allen",
              "link": "/api/v1/people/8474596"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 52,
          "y": 18
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM60",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (28) Slap Shot, assists: Ryan Nugent-Hopkins (22)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8479977,
              "fullName": "Kailer Yamamoto",
              "link": "/api/v1/people/8479977"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8474596,
              "fullName": "Jake Allen",
              "link": "/api/v1/people/8474596"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM656",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (29) Wrist Shot, assists: Kailer Yamamoto (4), Ryan Nugent-Hopkins (23)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8479977,
              "fullName": "Kailer Yamamoto",
              "link": "/api/v1/people/8479977"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8477424,
              "fullName": "Juuse Saros",
              "link": "/api/v1/people/8477424"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 76,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM530",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (30) Snap Shot, assists: Ryan Nugent-Hopkins (24), Kailer Yamamoto (7)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8476472,
              "fullName": "Oscar Klefbom",
              "link": "/api/v1/people/8476472"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8477424,
              "fullName": "Juuse Saros",
              "link": "/api/v1/people/8477424"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 64,
          "y": -28
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM702",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (31) Snap Shot, assists: Ryan Nugent-Hopkins (25), Oscar Klefbom (27)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 32
          }
        ],
        "coordinates": {
          "x": 56,
          "y": 32
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM768",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (32) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": true
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8479977,
              "fullName": "Kailer Yamamoto",
              "link": "/api/v1/people/8479977"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8478470,
              "fullName": "Sam Montembeault",
              "link": "/api/v1/people/8478470"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 70,
          "y": 8
        },
        "result": {
          "event": "Goal",
          "eventCode": "FLA464",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (33) Slap Shot, assists: Kailer Yamamoto (9), Ryan Nugent-Hopkins (29)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8476915,
              "fullName": "Jujhar Khaira",
              "link": "/api/v1/people/8476915"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8477498,
              "fullName": "Darnell Nurse",
              "link": "/api/v1/people/8477498"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8473503,
              "fullName": "James Reimer",
              "link": "/api/v1/people/8473503"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": -11
        },
        "result": {
          "event": "Goal",
          "eventCode": "CAR17",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (34) Wrist Shot, assists: Jujhar Khaira (3), Darnell Nurse (21)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8476988,
              "fullName": "Matt Benning",
              "link": "/api/v1/people/8476988"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8471774,
              "fullName": "Alex Stalock",
              "link": "/api/v1/people/8471774"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 90,
          "y": 17
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM279",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (35) Backhand, assists: Matt Benning (7), Ryan Nugent-Hopkins (31)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 52
          },
          {
            "player": {
              "id": 8478452,
              "fullName": "Caleb Jones",
              "link": "/api/v1/people/8478452"
            },
            "playerType": "Assist",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8477361,
              "fullName": "Calvin Petersen",
              "link": "/api/v1/people/8477361"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": -20
        },
        "result": {
          "event": "Goal",
          "eventCode": "LAK132",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (36) Slap Shot, assists: Connor McDavid (52), Caleb Jones (4)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 55
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -18
        },
        "result": {
          "event": "Goal",
          "eventCode": "ANA450",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (37) Wrist Shot, assists: Ryan Nugent-Hopkins (32), Connor McDavid (55)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 38
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 57
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -14
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM76",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (38) Snap Shot, assists: Connor McDavid (57), Ryan Nugent-Hopkins (33)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 58
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": 2
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM567",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (39) Tip-In, assists: Connor McDavid (58), Ryan Nugent-Hopkins (34)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 60
          },
          {
            "player": {
              "id": 8478451,
              "fullName": "Ethan Bear",
              "link": "/api/v1/people/8478451"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -20
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH213",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (40) Snap Shot, assists: Connor McDavid (60), Ethan Bear (16)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8479977,
              "fullName": "Kailer Yamamoto",
              "link": "/api/v1/people/8479977"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -5
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH435",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (41) Backhand, assists: Kailer Yamamoto (13), Ryan Nugent-Hopkins (36)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 42
          },
          {
            "player": {
              "id": 8476454,
              "fullName": "Ryan Nugent-Hopkins",
              "link": "/api/v1/people/8476454"
            },
            "playerType": "Assist",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": 18
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH445",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (42) Wrist Shot, assists: Ryan Nugent-Hopkins (37)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8477934,
              "fullName": "Leon Draisaitl",
              "link": "/api/v1/people/8477934"
            },
            "playerType": "Scorer",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8478402,
              "fullName": "Connor McDavid",
              "link": "/api/v1/people/8478402"
            },
            "playerType": "Assist",
            "seasonTotal": 62
          },
          {
            "player": {
              "id": 8477498,
              "fullName": "Darnell Nurse",
              "link": "/api/v1/people/8477498"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8471469,
              "fullName": "Pekka Rinne",
              "link": "/api/v1/people/8471469"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 73,
          "y": -7
        },
        "result": {
          "event": "Goal",
          "eventCode": "NSH448",
          "eventTypeId": "GOAL",
          "description": "Leon Draisaitl (43) Wrist Shot, assists: Connor McDavid (62), Darnell Nurse (24)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      }
    ]
  },
  {
    "name": "Mika Zibanejad",
    "id": 8476459,
    "team": "NYR",
    "teamLogo": "nyr-logo",
    "position": "C",
    "domId": "zibanejad",
    "isWinner": false,
    "missedGames": [
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22
    ],
    "totalGoals": 41,
    "gamesPlayed": 57,
    "gamesRemaining": 12,
    "gameArray": [
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020006,
          "link": "/api/v1/game/2019020006/feed/live",
          "content": {
            "link": "/api/v1/game/2019020006/content"
          }
        },
        "index": 56
      },
      {
        "goals": 3,
        "game": {
          "gamePk": 2019020020,
          "link": "/api/v1/game/2019020020/feed/live",
          "content": {
            "link": "/api/v1/game/2019020020/content"
          }
        },
        "index": 55
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020061,
          "link": "/api/v1/game/2019020061/feed/live",
          "content": {
            "link": "/api/v1/game/2019020061/content"
          }
        },
        "index": 54
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020100,
          "link": "/api/v1/game/2019020100/feed/live",
          "content": {
            "link": "/api/v1/game/2019020100/content"
          }
        },
        "index": 53
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020109,
          "link": "/api/v1/game/2019020109/feed/live",
          "content": {
            "link": "/api/v1/game/2019020109/content"
          }
        },
        "index": 52
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020124,
          "link": "/api/v1/game/2019020124/feed/live",
          "content": {
            "link": "/api/v1/game/2019020124/content"
          }
        },
        "index": 51
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020136,
          "link": "/api/v1/game/2019020136/feed/live",
          "content": {
            "link": "/api/v1/game/2019020136/content"
          }
        },
        "index": 50
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020147,
          "link": "/api/v1/game/2019020147/feed/live",
          "content": {
            "link": "/api/v1/game/2019020147/content"
          }
        },
        "index": 49
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020174,
          "link": "/api/v1/game/2019020174/feed/live",
          "content": {
            "link": "/api/v1/game/2019020174/content"
          }
        },
        "index": 48
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020381,
          "link": "/api/v1/game/2019020381/feed/live",
          "content": {
            "link": "/api/v1/game/2019020381/content"
          }
        },
        "index": 47
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020391,
          "link": "/api/v1/game/2019020391/feed/live",
          "content": {
            "link": "/api/v1/game/2019020391/content"
          }
        },
        "index": 46
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020403,
          "link": "/api/v1/game/2019020403/feed/live",
          "content": {
            "link": "/api/v1/game/2019020403/content"
          }
        },
        "index": 45
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020420,
          "link": "/api/v1/game/2019020420/feed/live",
          "content": {
            "link": "/api/v1/game/2019020420/content"
          }
        },
        "index": 44
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020444,
          "link": "/api/v1/game/2019020444/feed/live",
          "content": {
            "link": "/api/v1/game/2019020444/content"
          }
        },
        "index": 43
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020448,
          "link": "/api/v1/game/2019020448/feed/live",
          "content": {
            "link": "/api/v1/game/2019020448/content"
          }
        },
        "index": 42
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020466,
          "link": "/api/v1/game/2019020466/feed/live",
          "content": {
            "link": "/api/v1/game/2019020466/content"
          }
        },
        "index": 41
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020483,
          "link": "/api/v1/game/2019020483/feed/live",
          "content": {
            "link": "/api/v1/game/2019020483/content"
          }
        },
        "index": 40
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020498,
          "link": "/api/v1/game/2019020498/feed/live",
          "content": {
            "link": "/api/v1/game/2019020498/content"
          }
        },
        "index": 39
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020504,
          "link": "/api/v1/game/2019020504/feed/live",
          "content": {
            "link": "/api/v1/game/2019020504/content"
          }
        },
        "index": 38
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020520,
          "link": "/api/v1/game/2019020520/feed/live",
          "content": {
            "link": "/api/v1/game/2019020520/content"
          }
        },
        "index": 37
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020550,
          "link": "/api/v1/game/2019020550/feed/live",
          "content": {
            "link": "/api/v1/game/2019020550/content"
          }
        },
        "index": 36
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020565,
          "link": "/api/v1/game/2019020565/feed/live",
          "content": {
            "link": "/api/v1/game/2019020565/content"
          }
        },
        "index": 35
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020574,
          "link": "/api/v1/game/2019020574/feed/live",
          "content": {
            "link": "/api/v1/game/2019020574/content"
          }
        },
        "index": 34
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019020584,
          "link": "/api/v1/game/2019020584/feed/live",
          "content": {
            "link": "/api/v1/game/2019020584/content"
          }
        },
        "index": 33
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020593,
          "link": "/api/v1/game/2019020593/feed/live",
          "content": {
            "link": "/api/v1/game/2019020593/content"
          }
        },
        "index": 32
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020623,
          "link": "/api/v1/game/2019020623/feed/live",
          "content": {
            "link": "/api/v1/game/2019020623/content"
          }
        },
        "index": 31
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020635,
          "link": "/api/v1/game/2019020635/feed/live",
          "content": {
            "link": "/api/v1/game/2019020635/content"
          }
        },
        "index": 30
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020651,
          "link": "/api/v1/game/2019020651/feed/live",
          "content": {
            "link": "/api/v1/game/2019020651/content"
          }
        },
        "index": 29
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020666,
          "link": "/api/v1/game/2019020666/feed/live",
          "content": {
            "link": "/api/v1/game/2019020666/content"
          }
        },
        "index": 28
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020682,
          "link": "/api/v1/game/2019020682/feed/live",
          "content": {
            "link": "/api/v1/game/2019020682/content"
          }
        },
        "index": 27
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020698,
          "link": "/api/v1/game/2019020698/feed/live",
          "content": {
            "link": "/api/v1/game/2019020698/content"
          }
        },
        "index": 26
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020710,
          "link": "/api/v1/game/2019020710/feed/live",
          "content": {
            "link": "/api/v1/game/2019020710/content"
          }
        },
        "index": 25
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020730,
          "link": "/api/v1/game/2019020730/feed/live",
          "content": {
            "link": "/api/v1/game/2019020730/content"
          }
        },
        "index": 24
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020757,
          "link": "/api/v1/game/2019020757/feed/live",
          "content": {
            "link": "/api/v1/game/2019020757/content"
          }
        },
        "index": 23
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020762,
          "link": "/api/v1/game/2019020762/feed/live",
          "content": {
            "link": "/api/v1/game/2019020762/content"
          }
        },
        "index": 22
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020785,
          "link": "/api/v1/game/2019020785/feed/live",
          "content": {
            "link": "/api/v1/game/2019020785/content"
          }
        },
        "index": 21
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020796,
          "link": "/api/v1/game/2019020796/feed/live",
          "content": {
            "link": "/api/v1/game/2019020796/content"
          }
        },
        "index": 20
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020810,
          "link": "/api/v1/game/2019020810/feed/live",
          "content": {
            "link": "/api/v1/game/2019020810/content"
          }
        },
        "index": 19
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020825,
          "link": "/api/v1/game/2019020825/feed/live",
          "content": {
            "link": "/api/v1/game/2019020825/content"
          }
        },
        "index": 18
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020840,
          "link": "/api/v1/game/2019020840/feed/live",
          "content": {
            "link": "/api/v1/game/2019020840/content"
          }
        },
        "index": 17
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020857,
          "link": "/api/v1/game/2019020857/feed/live",
          "content": {
            "link": "/api/v1/game/2019020857/content"
          }
        },
        "index": 16
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020872,
          "link": "/api/v1/game/2019020872/feed/live",
          "content": {
            "link": "/api/v1/game/2019020872/content"
          }
        },
        "index": 15
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020887,
          "link": "/api/v1/game/2019020887/feed/live",
          "content": {
            "link": "/api/v1/game/2019020887/content"
          }
        },
        "index": 14
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020892,
          "link": "/api/v1/game/2019020892/feed/live",
          "content": {
            "link": "/api/v1/game/2019020892/content"
          }
        },
        "index": 13
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020907,
          "link": "/api/v1/game/2019020907/feed/live",
          "content": {
            "link": "/api/v1/game/2019020907/content"
          }
        },
        "index": 12
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020927,
          "link": "/api/v1/game/2019020927/feed/live",
          "content": {
            "link": "/api/v1/game/2019020927/content"
          }
        },
        "index": 11
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020942,
          "link": "/api/v1/game/2019020942/feed/live",
          "content": {
            "link": "/api/v1/game/2019020942/content"
          }
        },
        "index": 10
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020953,
          "link": "/api/v1/game/2019020953/feed/live",
          "content": {
            "link": "/api/v1/game/2019020953/content"
          }
        },
        "index": 9
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020971,
          "link": "/api/v1/game/2019020971/feed/live",
          "content": {
            "link": "/api/v1/game/2019020971/content"
          }
        },
        "index": 8
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019020985,
          "link": "/api/v1/game/2019020985/feed/live",
          "content": {
            "link": "/api/v1/game/2019020985/content"
          }
        },
        "index": 7
      },
      {
        "goals": 0,
        "game": {
          "gamePk": 2019020994,
          "link": "/api/v1/game/2019020994/feed/live",
          "content": {
            "link": "/api/v1/game/2019020994/content"
          }
        },
        "index": 6
      },
      {
        "goals": 2,
        "game": {
          "gamePk": 2019021011,
          "link": "/api/v1/game/2019021011/feed/live",
          "content": {
            "link": "/api/v1/game/2019021011/content"
          }
        },
        "index": 5
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021021,
          "link": "/api/v1/game/2019021021/feed/live",
          "content": {
            "link": "/api/v1/game/2019021021/content"
          }
        },
        "index": 4
      },
      {
        "goals": 5,
        "game": {
          "gamePk": 2019021036,
          "link": "/api/v1/game/2019021036/feed/live",
          "content": {
            "link": "/api/v1/game/2019021036/content"
          }
        },
        "index": 3
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021055,
          "link": "/api/v1/game/2019021055/feed/live",
          "content": {
            "link": "/api/v1/game/2019021055/content"
          }
        },
        "index": 2
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021076,
          "link": "/api/v1/game/2019021076/feed/live",
          "content": {
            "link": "/api/v1/game/2019021076/content"
          }
        },
        "index": 1
      },
      {
        "goals": 1,
        "game": {
          "gamePk": 2019021081,
          "link": "/api/v1/game/2019021081/feed/live",
          "content": {
            "link": "/api/v1/game/2019021081/content"
          }
        },
        "index": 0
      }
    ],
    "sumArray": [
      0,
      1,
      4,
      4,
      4,
      4,
      4,
      4,
      4,
      4,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      5,
      6,
      6,
      6,
      6,
      7,
      7,
      9,
      11,
      11,
      11,
      12,
      12,
      14,
      15,
      16,
      16,
      16,
      17,
      17,
      17,
      17,
      18,
      18,
      18,
      19,
      20,
      20,
      21,
      22,
      22,
      23,
      24,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      30,
      32,
      33,
      38,
      39,
      40,
      41
    ],
    "scoringPlays": [
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8476885,
              "fullName": "Jacob Trouba",
              "link": "/api/v1/people/8476885"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8479333,
              "fullName": "Libor Hajek",
              "link": "/api/v1/people/8479333"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR701",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (1) Backhand, assists: Jacob Trouba (2), Libor Hajek (1)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8476885,
              "fullName": "Jacob Trouba",
              "link": "/api/v1/people/8476885"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": -3
        },
        "result": {
          "event": "Goal",
          "eventCode": "OTT34",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (2) Wrist Shot, assists: Chris Kreider (2), Jacob Trouba (3)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "OTT516",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (3) Snap Shot, assists: Pavel Buchnevich (1), Artemi Panarin (2)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 4
          },
          {
            "player": {
              "id": 8474090,
              "fullName": "Brendan Smith",
              "link": "/api/v1/people/8474090"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8467950,
              "fullName": "Craig Anderson",
              "link": "/api/v1/people/8467950"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 74,
          "y": -9
        },
        "result": {
          "event": "Goal",
          "eventCode": "OTT528",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (4) Snap Shot, assists: Brendan Smith (1)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "SHG",
            "name": "Short Handed"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 5
          },
          {
            "player": {
              "id": 8479323,
              "fullName": "Adam Fox",
              "link": "/api/v1/people/8479323"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8475852,
              "fullName": "Petr Mrazek",
              "link": "/api/v1/people/8475852"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 87,
          "y": 7
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR66",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (5) Wrist Shot, assists: Adam Fox (9), Artemi Panarin (19)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8477962,
              "fullName": "Brendan Lemieux",
              "link": "/api/v1/people/8477962"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "NJD581",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (6) Snap Shot, assists: Brendan Lemieux (7)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "SHG",
            "name": "Short Handed"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8481554,
              "fullName": "Kaapo Kakko",
              "link": "/api/v1/people/8481554"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8476876,
              "fullName": "Malcolm Subban",
              "link": "/api/v1/people/8476876"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 94,
          "y": 12
        },
        "result": {
          "event": "Goal",
          "eventCode": "VGK792",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (7) Tip-In, assists: Kaapo Kakko (7), Artemi Panarin (22)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8474889,
              "fullName": "Martin Jones",
              "link": "/api/v1/people/8474889"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 46,
          "y": -17
        },
        "result": {
          "event": "Goal",
          "eventCode": "SJS499",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (8) Wrist Shot, assists: Tony DeAngelo (15), Artemi Panarin (23)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8475855,
              "fullName": "Jesper Fast",
              "link": "/api/v1/people/8475855"
            },
            "playerType": "Assist",
            "seasonTotal": 7
          },
          {
            "player": {
              "id": 8474889,
              "fullName": "Martin Jones",
              "link": "/api/v1/people/8474889"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 58,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "SJS659",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (9) Wrist Shot, assists: Chris Kreider (9), Jesper Fast (7)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 10
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": 3
        },
        "result": {
          "event": "Goal",
          "eventCode": "ANA103",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (10) Backhand, assists: none",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": 14
        },
        "result": {
          "event": "Goal",
          "eventCode": "ANA544",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (11) Snap Shot, assists: Tony DeAngelo (16)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 0
          },
          {
            "player": {
              "id": 8476434,
              "fullName": "John Gibson",
              "link": "/api/v1/people/8476434"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 79,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "ANA779",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad - Wrist Shot",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8476458,
              "fullName": "Ryan Strome",
              "link": "/api/v1/people/8476458"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8468685,
              "fullName": "Henrik Lundqvist",
              "link": "/api/v1/people/8468685"
            },
            "playerType": "Assist",
            "seasonTotal": 1
          },
          {
            "player": {
              "id": 8468011,
              "fullName": "Ryan Miller",
              "link": "/api/v1/people/8468011"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 84,
          "y": 15
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR753",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (12) Wrist Shot, assists: Ryan Strome (23), Henrik Lundqvist (1)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "SHG",
            "name": "Short Handed"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8473503,
              "fullName": "James Reimer",
              "link": "/api/v1/people/8473503"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": -8
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR100",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (13) Snap Shot, assists: Chris Kreider (12), Tony DeAngelo (19)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8473503,
              "fullName": "James Reimer",
              "link": "/api/v1/people/8473503"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 75,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR507",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (14) Wrist Shot, assists: Tony DeAngelo (20), Artemi Panarin (26)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 15
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8475883,
              "fullName": "Frederik Andersen",
              "link": "/api/v1/people/8475883"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 70,
          "y": -21
        },
        "result": {
          "event": "Goal",
          "eventCode": "TOR499",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (15) Wrist Shot, assists: Chris Kreider (13)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 16
          },
          {
            "player": {
              "id": 8479323,
              "fullName": "Adam Fox",
              "link": "/api/v1/people/8479323"
            },
            "playerType": "Assist",
            "seasonTotal": 14
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475156,
              "fullName": "Mikko Koskinen",
              "link": "/api/v1/people/8475156"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 54,
          "y": -25
        },
        "result": {
          "event": "Goal",
          "eventCode": "EDM736",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (16) Snap Shot, assists: Adam Fox (14), Tony DeAngelo (21)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8475855,
              "fullName": "Jesper Fast",
              "link": "/api/v1/people/8475855"
            },
            "playerType": "Assist",
            "seasonTotal": 9
          },
          {
            "player": {
              "id": 8475831,
              "fullName": "Philipp Grubauer",
              "link": "/api/v1/people/8475831"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 77,
          "y": -2
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR38",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (17) Slap Shot, assists: Tony DeAngelo (23), Jesper Fast (9)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8473575,
              "fullName": "Semyon Varlamov",
              "link": "/api/v1/people/8473575"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYI455",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (18) Wrist Shot, assists: none",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 19
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 43
          },
          {
            "player": {
              "id": 8470657,
              "fullName": "Jimmy Howard",
              "link": "/api/v1/people/8470657"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 52,
          "y": 21
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR443",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (19) Wrist Shot, assists: Tony DeAngelo (27), Artemi Panarin (43)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 20
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 17
          },
          {
            "player": {
              "id": 8476458,
              "fullName": "Ryan Strome",
              "link": "/api/v1/people/8476458"
            },
            "playerType": "Assist",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8473541,
              "fullName": "Jonathan Bernier",
              "link": "/api/v1/people/8473541"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 3
        },
        "result": {
          "event": "Goal",
          "eventCode": "DET75",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (20) Wrist Shot, assists: Chris Kreider (17), Ryan Strome (32)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 18
          },
          {
            "player": {
              "id": 8474636,
              "fullName": "Michael Hutchinson",
              "link": "/api/v1/people/8474636"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 56,
          "y": 20
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR100",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (21) Snap Shot, assists: Chris Kreider (18)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 45
          },
          {
            "player": {
              "id": 8471686,
              "fullName": "Marc Staal",
              "link": "/api/v1/people/8471686"
            },
            "playerType": "Assist",
            "seasonTotal": 6
          },
          {
            "player": {
              "id": 8475622,
              "fullName": "Carter Hutton",
              "link": "/api/v1/people/8475622"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 86,
          "y": -6
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR713",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (22) Tip-In, assists: Artemi Panarin (45), Marc Staal (6)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 22
          },
          {
            "player": {
              "id": 8476945,
              "fullName": "Connor Hellebuyck",
              "link": "/api/v1/people/8476945"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 69,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "WPG418",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (23) Wrist Shot, assists: Pavel Buchnevich (22)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 47
          },
          {
            "player": {
              "id": 8479323,
              "fullName": "Adam Fox",
              "link": "/api/v1/people/8479323"
            },
            "playerType": "Assist",
            "seasonTotal": 24
          },
          {
            "player": {
              "id": 8471774,
              "fullName": "Alex Stalock",
              "link": "/api/v1/people/8471774"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 72,
          "y": -4
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN714",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (24) Tip-In, assists: Artemi Panarin (47), Adam Fox (24)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 0
          },
          {
            "player": {
              "id": 8471774,
              "fullName": "Alex Stalock",
              "link": "/api/v1/people/8471774"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": 4
        },
        "result": {
          "event": "Goal",
          "eventCode": "MIN814",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad - Backhand",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 49
          },
          {
            "player": {
              "id": 8476458,
              "fullName": "Ryan Strome",
              "link": "/api/v1/people/8476458"
            },
            "playerType": "Assist",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8470860,
              "fullName": "Jaroslav Halak",
              "link": "/api/v1/people/8470860"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 37,
          "y": 2
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR839",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (25) Wrist Shot, assists: Artemi Panarin (49), Ryan Strome (36)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 23
          },
          {
            "player": {
              "id": 8475184,
              "fullName": "Chris Kreider",
              "link": "/api/v1/people/8475184"
            },
            "playerType": "Assist",
            "seasonTotal": 21
          },
          {
            "player": {
              "id": 8475215,
              "fullName": "Robin Lehner",
              "link": "/api/v1/people/8475215"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 83,
          "y": -7
        },
        "result": {
          "event": "Goal",
          "eventCode": "CHI706",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (26) Tip-In, assists: Pavel Buchnevich (23), Chris Kreider (21)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8475852,
              "fullName": "Petr Mrazek",
              "link": "/api/v1/people/8475852"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 81,
          "y": 3
        },
        "result": {
          "event": "Goal",
          "eventCode": "CAR215",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (27) Backhand, assists: none",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 52
          },
          {
            "player": {
              "id": 8477180,
              "fullName": "Aaron Dell",
              "link": "/api/v1/people/8477180"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": 7
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR413",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (28) Snap Shot, assists: Artemi Panarin (52)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 53
          },
          {
            "player": {
              "id": 8473575,
              "fullName": "Semyon Varlamov",
              "link": "/api/v1/people/8473575"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 59,
          "y": 1
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYI670",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (29) Slap Shot, assists: Artemi Panarin (53)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 54
          },
          {
            "player": {
              "id": 8479328,
              "fullName": "Julien Gauthier",
              "link": "/api/v1/people/8479328"
            },
            "playerType": "Assist",
            "seasonTotal": 2
          },
          {
            "player": {
              "id": 8471679,
              "fullName": "Carey Price",
              "link": "/api/v1/people/8471679"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 68,
          "y": -19
        },
        "result": {
          "event": "Goal",
          "eventCode": "MTL660",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (30) Wrist Shot, assists: Artemi Panarin (54), Julien Gauthier (2)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 57
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8479394,
              "fullName": "Carter Hart",
              "link": "/api/v1/people/8479394"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 72,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR339",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (31) Tip-In, assists: Artemi Panarin (57), Tony DeAngelo (35)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 32
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 58
          },
          {
            "player": {
              "id": 8479353,
              "fullName": "Brett Howden",
              "link": "/api/v1/people/8479353"
            },
            "playerType": "Assist",
            "seasonTotal": 8
          },
          {
            "player": {
              "id": 8479394,
              "fullName": "Carter Hart",
              "link": "/api/v1/people/8479394"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 84,
          "y": 8
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR542",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (32) Backhand, assists: Artemi Panarin (58), Brett Howden (8)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8476458,
              "fullName": "Ryan Strome",
              "link": "/api/v1/people/8476458"
            },
            "playerType": "Assist",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 25
          },
          {
            "player": {
              "id": 8476412,
              "fullName": "Jordan Binnington",
              "link": "/api/v1/people/8476412"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 82,
          "y": 10
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR73",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (33) Slap Shot, assists: Ryan Strome (41), Pavel Buchnevich (25)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 34
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 59
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8478492,
              "fullName": "Ilya Samsonov",
              "link": "/api/v1/people/8478492"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": -3
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR75",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (34) Tip-In, assists: Artemi Panarin (59), Tony DeAngelo (37)",
          "secondaryType": "Tip-In",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 35
          },
          {
            "player": {
              "id": 8479323,
              "fullName": "Adam Fox",
              "link": "/api/v1/people/8479323"
            },
            "playerType": "Assist",
            "seasonTotal": 29
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 26
          },
          {
            "player": {
              "id": 8478492,
              "fullName": "Ilya Samsonov",
              "link": "/api/v1/people/8478492"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 61,
          "y": 32
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR321",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (35) Slap Shot, assists: Adam Fox (29), Pavel Buchnevich (26)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 36
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 27
          },
          {
            "player": {
              "id": 8478492,
              "fullName": "Ilya Samsonov",
              "link": "/api/v1/people/8478492"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 85,
          "y": 7
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR522",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (36) Snap Shot, assists: Pavel Buchnevich (27)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 37
          },
          {
            "player": {
              "id": 8481554,
              "fullName": "Kaapo Kakko",
              "link": "/api/v1/people/8481554"
            },
            "playerType": "Assist",
            "seasonTotal": 13
          },
          {
            "player": {
              "id": 8477962,
              "fullName": "Brendan Lemieux",
              "link": "/api/v1/people/8477962"
            },
            "playerType": "Assist",
            "seasonTotal": 11
          },
          {
            "player": {
              "id": 8478492,
              "fullName": "Ilya Samsonov",
              "link": "/api/v1/people/8478492"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 78,
          "y": -13
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR634",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (37) Slap Shot, assists: Kaapo Kakko (13), Brendan Lemieux (11)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "PPG",
            "name": "Power Play"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 38
          },
          {
            "player": {
              "id": 8478550,
              "fullName": "Artemi Panarin",
              "link": "/api/v1/people/8478550"
            },
            "playerType": "Assist",
            "seasonTotal": 61
          },
          {
            "player": {
              "id": 8477950,
              "fullName": "Tony DeAngelo",
              "link": "/api/v1/people/8477950"
            },
            "playerType": "Assist",
            "seasonTotal": 38
          },
          {
            "player": {
              "id": 8478492,
              "fullName": "Ilya Samsonov",
              "link": "/api/v1/people/8478492"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 83,
          "y": 5
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR642",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (38) Backhand, assists: Artemi Panarin (61), Tony DeAngelo (38)",
          "secondaryType": "Backhand",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": true,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 39
          },
          {
            "player": {
              "id": 8479323,
              "fullName": "Adam Fox",
              "link": "/api/v1/people/8479323"
            },
            "playerType": "Assist",
            "seasonTotal": 31
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 28
          },
          {
            "player": {
              "id": 8478406,
              "fullName": "Mackenzie Blackwood",
              "link": "/api/v1/people/8478406"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 66,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "NYR87",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (39) Slap Shot, assists: Adam Fox (31), Pavel Buchnevich (28)",
          "secondaryType": "Slap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 40
          },
          {
            "player": {
              "id": 8477962,
              "fullName": "Brendan Lemieux",
              "link": "/api/v1/people/8477962"
            },
            "playerType": "Assist",
            "seasonTotal": 12
          },
          {
            "player": {
              "id": 8477402,
              "fullName": "Pavel Buchnevich",
              "link": "/api/v1/people/8477402"
            },
            "playerType": "Assist",
            "seasonTotal": 30
          },
          {
            "player": {
              "id": 8471750,
              "fullName": "Ben Bishop",
              "link": "/api/v1/people/8471750"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 80,
          "y": -1
        },
        "result": {
          "event": "Goal",
          "eventCode": "DAL74",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (40) Snap Shot, assists: Brendan Lemieux (12), Pavel Buchnevich (30)",
          "secondaryType": "Snap Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      },
      {
        "players": [
          {
            "player": {
              "id": 8476459,
              "fullName": "Mika Zibanejad",
              "link": "/api/v1/people/8476459"
            },
            "playerType": "Scorer",
            "seasonTotal": 41
          },
          {
            "player": {
              "id": 8476858,
              "fullName": "Phillip Di Giuseppe",
              "link": "/api/v1/people/8476858"
            },
            "playerType": "Assist",
            "seasonTotal": 3
          },
          {
            "player": {
              "id": 8479323,
              "fullName": "Adam Fox",
              "link": "/api/v1/people/8479323"
            },
            "playerType": "Assist",
            "seasonTotal": 33
          },
          {
            "player": {
              "id": 8480925,
              "fullName": "Pavel Francouz",
              "link": "/api/v1/people/8480925"
            },
            "playerType": "Goalie"
          }
        ],
        "coordinates": {
          "x": 62,
          "y": -19
        },
        "result": {
          "event": "Goal",
          "eventCode": "COL75",
          "eventTypeId": "GOAL",
          "description": "Mika Zibanejad (41) Wrist Shot, assists: Phillip Di Giuseppe (3), Adam Fox (33)",
          "secondaryType": "Wrist Shot",
          "strength": {
            "code": "EVEN",
            "name": "Even"
          },
          "gameWinningGoal": false,
          "emptyNet": false
        }
      }
    ]
  }
];